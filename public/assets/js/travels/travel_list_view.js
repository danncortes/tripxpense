$(function(){

	sortShape($('.sort-shape'), $('.travel-items'), makeIsotope, positionFooter)
	sortDirection($('.sort-direction'), $('.sort-type'))

	/** Sorting by type of item **/
	sortType($('.sort-type'), $('.sort-direction'))

	/** isotope function **/
	makeIsotope()

	/** Change from block to list items depending of resolution **/
	transformTypeList($('.travel-items'))

	$(window).resize(function(){
		transformTypeList($('.travel-items'))
	})

	/** isotope function **/
	function makeIsotope(){
		$('.isotope').isotope({
			itemSelector: '.elem-item',
			layoutMode: 'masonry',
			getSortData: {
				name: function(itemElem) {
	        var name = $( itemElem ).find('.name').text();
	        return name.toLowerCase()
	      },
				start: '.start',
				finish: '.finish',
				creditcard: function(itemElem) {
	        var creditcard = $( itemElem ).find('.creditcard').text();
	        return parseInt(creditcard.replace("$ ", ""));
	      },
				cash: function(itemElem) {
	        var cash = $( itemElem ).find('.cash').text();
	        return parseInt(cash.replace("$ ", ""));
	      }
			}
		})
	}
	
})