$(function(){

	removePic()

	var spinner='<i class="fa fa-spinner fa-spin"></i>'

	function removePic(){

		var formPicCategory=$('.form-picture-category')
		var editPicCategoryArea=formPicCategory.find('.edit-pic-category-area')
		var editPicCategory=formPicCategory.find('.edit-pic-category')
		var removeButton=editPicCategory.find('.button')
		var idElem=removeButton.attr('data-id')

		removeButton.on('click', function(){
			var urlFind = "/removePic/"+idElem+"";
			confirm('The icon will be deleted, Are you sure you want to do it?',
				removePicRoute(urlFind, editPicCategory, editPicCategoryArea))
		})
	};

	function removePicRoute(urlFind, editPicCategory, editPicCategoryArea){

		$.ajax(
		{
	    url: urlFind,
	    type: 'post',
	    beforeSend: function(){
	    	editPicCategoryArea.append(spinner)
    	},
	    success: function() {
	    	editPicCategory.remove()
    	},
    	complete: function(){
    		editPicCategoryArea.find('.fa-spinner').remove()
    		editPicCategoryArea.find('input').removeClass('hidden')
    	}
		})
	}



})