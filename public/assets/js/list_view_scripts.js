
/** Change from list to block elements depending of resolution **/

function transformTypeList(elem){
	widthDocu=$(document).width()
	operationItems=elem
	if($('#item-list').is(":checked")){
		if(widthDocu<768){
			operationItems.removeClass('list')
			$('.sort-shape').hide()
		}
		else{
			operationItems.addClass('list')
			$('.sort-shape').show()
		}
	}
	else{
		if(widthDocu<768){
			$('.sort-shape').hide()
		}
		else{
			$('.sort-shape').show()
		}
	}
}

/** End Change from list to block elements depending of resolution **/

/** Sorting by type of item **/

function sortType(areaType, areaDir){
	areaType.on( 'change', 'input', function() {
    var sortValue = $(this).attr('data-sort-value');
    itemDirChecked=areaDir.find(':checked').attr('data-sort-value')
    console.debug(itemDirChecked)

    var sortDirect

    if(itemDirChecked=='asc'){
    	sortDirect=true
    }
    else{
    	sortDirect=false
    }
    $('.isotope').isotope({ sortBy: sortValue,  sortAscending: sortDirect});
  });
}

/** End Sorting by type of item **/

/** Sorting by shape, block or list item **/

function sortShape(sortShape, seccItem, makeIsotope, positionFooter){
	checkElem=sortShape.find('input')
	checkBlock=sortShape.find('#item-block')
	checkList=sortShape.find('#item-list')

	typeItem()

	checkElem.on('change', typeItem)

	function typeItem(){
		if(checkBlock.is(":checked")){
			seccItem.removeClass('list')
			makeIsotope()
			positionFooter()
		}
		else{
			seccItem.addClass('list')
			makeIsotope()
			positionFooter()
		}
	}
}

/** End Sorting by shape, block or list item **/

/** Sorting by direction asc or desc item **/

function sortDirection(areaDir, areaType){

		checkElemDir=areaDir.find('input')
		checkUpDown=areaDir.find('#item-updown')
		checkDownUp=areaDir.find('#item-downup')

		changeDirectionSort()

		checkElemDir.on('change', changeDirectionSort)

		function changeDirectionSort(){
			itemChecked=areaType.find(':checked').attr('data-sort-value')

			if(checkUpDown.is(":checked")){
				$('.isotope').isotope({
				  sortBy: itemChecked,
				  sortAscending: true
				})
			}
			else{
				$('.isotope').isotope({
				  sortBy: itemChecked,
				  sortAscending: false
				})
			}
		}
	}

/** End Sorting by direction asc or desc item **/

function positionFooter(){
	footer=$('footer')
	body=$('body')
	docu=$(document)
	windo=$(window)
	bodyHeight=body.height()
	windoHeight=windo.height()
	if(bodyHeight<windoHeight){
		footer.addClass('fixed bottom w100')
	}else{
		footer.removeClass('fixed bottom w100')
	}
}