$(function(){

	startCreateOperation()
	startCreateTravel()

	var docum = $(document)
  var windo = $(window)
  var header = $('header')

	/*HEADER TRANSFORM*/
	docum.scroll(function(){
		disTop=$(this).scrollTop()
		if(disTop>=90){
			$('header .container-fluid').removeClass('normal-h')
			$('header .container-fluid').addClass('slim-h')
		}else{
			$('header .container-fluid').removeClass('slim-h')
			$('header .container-fluid').addClass('normal-h')
		}
	})
	/*HEADER TRANSFORM*/

	/*FOOTER TRANSFORM*/
	positionFooter()

	function positionFooter(){
		footer=$('footer')
		body=$('body')
		docu=$(document)
		windo=$(window)
		bodyHeight=body.height()
		windoHeight=windo.height()

		if(bodyHeight<windoHeight){
			footer.addClass('fixed bottom w100')
		}else{
			footer.removeClass('fixed bottom w100')
		}
	}
	/*FOOTER TRANSFORM*/

	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

	$('.autor').popover({
		html : true,
	})

	$('.datepicker input').datepicker({
		format: 'yyyy-mm-dd',
	})
	$('.carousel').carousel()
	$('.select').selectpicker();


	windo.resize(function(){
		positionFooter()
	})

	

	function startCreateTravel(){
		$('.newTravelButton').on('click', function(){
			modalStart=$('#startingTravel')
			$('#startingTravel').modal('show')
		})
	}

	function startCreateOperation(){
		$('.newOperationButton').on('click', function(){
			modalStart=$('#startingOperation')
			contentModal=modalStart.find('.content')
			htmlCont=!$.trim($('#startingOperation .modal-body .content').html())

			if(htmlCont==true){
				$.ajax(
				{
			    url: "/startOperation",
			    type: 'POST',
			    dataType: 'json',
			    beforeSend: function(){
			    	$('.newOperationButton .fa-plus').addClass('hidden')
			    	$('.newOperationButton').append('<i class="fa fa-spinner fa-pulse"></i>')
		    	},
			    success: function(data) {
			    	contentModal.append('<h4 class="title">Category</h4>')
			    	contentModal.append('<div class="category-items"><ul class="visible-area mini"></ul></div>')
			    	categoryElementList=contentModal.find('.category-items .visible-area')
			    	for(i=0; i<data.categories.length; i++){
			    		categoryElementList.append(
			    			'<li>'+
			    				'<input id="cat-'+i+'" type="radio" name="category" value="'+data.categories[i].id+'" required />'+
			    				'<label for="cat-'+i+'" class="">'+
										'<div class="buttonCont">'+
											'<div class="icon">'+
												'<img src="'+data.url+data.categories[i].pic+'"/>'+
											'</div>'+
											'<div class="cap">'+data.categories[i].name+'</div>'+
										'</div>'+
									'</label>'+
			    			'</li>'
			    		)
			    	}
			    	contentModal.append('<div class="view-more-cat">+ Categories</div>')
			    	contentModal.append('<h4 class="title">Payment method</h4>')
			    	contentModal.append('<div class="methods-items"><ul></ul></div>')
			    	methodsElementList=contentModal.find('.methods-items ul')
			    	for(i=0; i<data.pay_methods.length; i++){
			    		icon=''
			    		if(data.pay_methods[i].name == 'CASH'){icon='fa fa-money cash'}
			    		else if(data.pay_methods[i].name == 'CREDIT CARD'){icon='fa fa-credit-card credit'}
			    		else if(data.pay_methods[i].name == 'DEBIT CARD'){icon='fa fa-credit-card debit'}
			    		methodsElementList.append(
			    			'<li>'+
			    				'<input id="method-'+i+'" type="radio" name="pay_methods" value="'+data.pay_methods[i].id+'" required/>'+
			    				'<label for="method-'+i+'" class="">'+
										'<div class="buttonCont">'+
											'<div class="icon">'+
												'<i class="'+icon+' fa-2x"></i>'+
											'</div>'+
											'<div class="cap">'+data.pay_methods[i].name+'</div>'+
										'</div>'+
									'</label>'+
			    			'</li>'
			    		)
			    	}
			    	contentModal.append('<h4 class="title">Travel</h4>')
			    	contentModal.append(
			    		'<select id="travel" class="select" data-width="100%" name="travel" required>'+
			    		'</select>'
			    	)
			    	selectTravel=contentModal.find('#travel')
			    	if(data.travels.length>1){
			    		selectTravel.append('<option value="">-</option>')
			    	}
			    	for(i=0; i<data.travels.length; i++){
			    		selectTravel.append(
			    			'<option value="'+data.travels[i].id+'">'+data.travels[i].name+'</option>'
			    		)
			    	}
		    	},
		    	complete: function(){
		    		$('.newOperationButton .fa-plus').removeClass('hidden')
			    	$('.newOperationButton .fa-spinner').addClass('hidden')
		    		$('#startingOperation').modal('show')
		    		$('.select').selectpicker('refresh');
		    		$('.view-more-cat').on('click', function(){
		    			if($('.visible-area').hasClass('mini')){
		    				$('.visible-area').removeClass('mini')
		    				$(this).html('- Categories')
		    			}
		    			else{
		    				$('.visible-area').addClass('mini')
		    				$(this).html('+ Categories')
		    			}
		    		})
		    	}
				})
			}
			else{
		    $('#startingOperation').modal('show')
			}
		})
	}
})