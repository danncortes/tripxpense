$(function(){

	sortShape($('.sort-shape'), $('.operation-items'), makeIsotope, positionFooter)
	sortDirection($('.sort-direction'), $('.sort-type'))

	/** Sorting by type of item **/
	sortType($('.sort-type'), $('.sort-direction'))

	/** isotope function **/
	// makeIsotope()

	/** Change from block to list items depending of resolution **/
	transformTypeList($('.operation-items'))

	$(window).resize(function(){
		transformTypeList($('.operation-items'))
	})

	/** isotope function **/
	function makeIsotope(){
		$('.isotope').isotope({
			itemSelector: '.elem-item',
			layoutMode: 'masonry',
			getSortData: {
				date: '.date',
				// title: '.title',
				title: function(itemElem) {
	        var title = $( itemElem ).find('.title').text();
	        return title.toLowerCase();
	      },
				spent: function(itemElem) {
	        var spent = $( itemElem ).find('.spent').text();
	        return parseInt(spent.replace("$ ", ""));
	      },
				travel: '.travel',
				type: '.type',
			}
		})
	}

	
})