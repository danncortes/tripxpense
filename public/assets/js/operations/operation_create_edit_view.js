$(function(){

	var lat, lon;

	neighborhood_field=$('.neighborhood_field');
	neighborhood_field_create=$('.neighborhood_field.create');
	neighborhood_field_data=neighborhood_field_create.val()
	
	getDirectionUser()

	if(neighborhood_field_data==""){
		// general function for get position and autocomplete selects
		getPositionUser()
	}
	// create a new object geocoder google api
	var geocoder = new google.maps.Geocoder()


	function getPositionUser(){
		// using the geolocation method that if the user allow the geolocation this return a success state for execute 'info' and a noSuccess
		navigator.geolocation.getCurrentPosition(info, noSuccess)
	}

	// success function that return lat and log
	function info(geo){
		lat = geo.coords.latitude
		lon = geo.coords.longitude

		// set the lat and long vars that will be used inside the getDirectionUserFromCoords() function
		yourLocation = new google.maps.LatLng( lat, lon )

		getDirectionUserFromCoords()
	}

	function noSuccess(error) {
	}

	getDirectionUserFromCoords = function(){
		// passed the coordinates and a function
		geocoder.geocode({ 'latLng': yourLocation}, processGeocoderCoords);
	}
	// this function returns a status and a results
	function processGeocoderCoords(results, status){
		if (status == google.maps.GeocoderStatus.OK) {

			geoloc={}

			results=results[0].address_components

			$.each(results, function(i){
				dataType=results[i].types[0]
				valueType=results[i].long_name

				geoloc[dataType]=valueType
			})
			// from the json results we get the interest data

			// Neighborhood
			if(geoloc.neighborhood != undefined){
				neighborhood = geoloc.neighborhood
			}
			else if(geoloc.sublocality_level_1 != undefined){
				neighborhood = geoloc.sublocality_level_1
			}
			else{
				neighborhood = $('.neighborhood_field').val()
			}

			//city
			city 					= geoloc.locality
			//country
			country				= geoloc.country

			$('.neighborhood_field').val(neighborhood)
			$('#cod_country').children('option').each(function(){
				content=$(this).html()
				if(content==country){
					theVal=$(this).attr('value')
					$('#cod_country').selectpicker('val', theVal);

					var selectCountry=$('#cod_country')
					var cityArea=$('#city-list-area')
					var cityArea_list=cityArea.find('select.city-list')
					var cityArea_col=cityArea.find('.city-col')
					var bootstrap_select='.city-list';
					var message1='country'
					var message="<div class='message'><p>There are no cities in this country</p></div>"
					var id='ID'
					var name='Name'
					var valor=theVal
					var urlFind = "/getcities/"+valor+"";
					var autoSelectCity=true
					getDatatoSelect(valor, urlFind, cityArea_col, cityArea_list, message, bootstrap_select, id, name, message1, autoSelectCity)

				}
			})
		}
	}

	function getDirectionUser(){
		neighborhood_field.on('change', function(){
			direction= $(this).val()
			geocoder.geocode({ 'address': direction}, processGeocoderCoords);
		})

	}



	// getCommerces()
	getCity()

	var spinner='<i class="fa fa-spinner fa-spin"></i>'

	// function getCommerces(){

	// 	var selectCategory=$('#cod_category')
	// 	var commerceArea=$('#commerce-list-area')
	// 	var commerceArea_col=commerceArea.find('.commerce-col')
	// 	var commerceArea_list=commerceArea.find('select.commerce-list')
	// 	var bootstrap_select='.commerce-list';
	// 	var message1='category'
	// 	var message="<div class='message'><p>There are no commerces in this category</p> <a href='/commerce/create' class='btn btn-success btn-xs'>Create a commerce</a></div>"
	// 	var id='id'
	// 	var name='name'

	// 	actionGetCommerce()

	// 	selectCategory.change(function(){
	// 		actionGetCommerce()
	// 	})

	// 	function actionGetCommerce(){
	// 		var valor=selectCategory.val()
	// 		var urlFind = "/findcommerce/"+valor+"";
	// 		var autoSelectCity=false
	// 		getDatatoSelect(valor, urlFind, commerceArea_col, commerceArea_list, message, bootstrap_select, id, name, message1, autoSelectCity)
	// 	}
	// };

	function getCity(){

		var selectCountry=$('#cod_country')
		var cityArea=$('#city-list-area')
		var cityArea_list=cityArea.find('select.city-list')
		var cityArea_col=cityArea.find('.city-col')
		var bootstrap_select='.city-list';
		var message1='country'
		var message="<div class='message'><p>There are no cities in this country</p></div>"
		var id='ID'
		var name='Name'

		selectCountry.change(function(){
			var valor=selectCountry.val()
			var urlFind = "/getcities/"+valor+"";
			var autoSelectCity=false
			getDatatoSelect(valor, urlFind, cityArea_col, cityArea_list, message, bootstrap_select, id, name, message1, autoSelectCity)
		})
	};

	function getDatatoSelect(valor, urlFind, column, selectItem, message, bootstrap_select, id, name, message1, autoSelectCity){
		if(valor!=''){
			$.ajax(
			{
		    url: urlFind,
		    type: 'post',
		    dataType: 'json',
		    data: {id: valor},
		    beforeSend: function(){
		    	column.append(spinner)
	    	},
		    success: function(data) {
		    	column.find('.fa').remove()
		    	column.find('.message').remove()
					selectItem.html('')
					selectItem.append('<option value="" selected>-</option>')
		    	if(data!=''){
			    	for (i = 0; i < data.length; i++) {
					    selectItem.append('<option value='+data[i][id]+'>'+data[i][name]+'</option>')
						}
						selectItem.selectpicker('show');
		    	}else{
		    		selectItem.hide()
		    		// selectItem.addClass('hidden')
	    			$(bootstrap_select).selectpicker('hide');
	    			column.append(message)
		    	}
	    	},
	    	complete: function(){
	    		selectItem.selectpicker('refresh');

	    		if(autoSelectCity==true){

	    			$('#cod_city').children('option').each(function(){
							content=$(this).html()
							if(content==city){
								theVal=$(this).attr('value')
								$('#cod_city').selectpicker('val', theVal);
							}
						})

	    		}
	    	}
			})
		}
		else{
			column.find('.message').remove()
			$('.select').selectpicker('show');
			selectItem.html('')
			selectItem.append('<option value="" selected>Select a '+message1+' first</option>')
			$('.select').selectpicker('refresh')
		}
	}


	// AUTOCOMPLETE

	$('#cod_commerce1').keyup(function(){
		// console.log($(this).val())
		if($(this).val().length>0){
			$("input[name='title']").val($(this).val())
		}
		else{
			$( "#cod_commerce" ).val('')
		  $('.create-commerce').addClass('disabled btn-default').removeClass('btn-success')
		}
	})

	getCommerces2()
	
	function getCommerces2(){
	  cod_category=$('#cod_category').val()

	  lookForCommerce(cod_category)

	  $('#cod_category').change(function(){
		  cod_category=$('#cod_category').val()
	  	lookForCommerce(cod_category)

	  	$('#cod_commerce1').val('')

		})

	  function lookForCommerce(cod_category){
	  	if(cod_category != ''){
			  $.ajax({
		      url: '/findcommerce/'+cod_category+'',
		      type: 'post',
		      dataType: 'json',
		      success: function (data) {
		      	newData=[]
		      	for(i = 0; i < data.length; i++){
		      	 item={'value':data[i].name, 'label':data[i].name, 'data':data[i].id}
		      	 newData.push(item)
		      	}
		    		
		      },
		      complete: function (){
		      	$( "#cod_commerce1" ).autocomplete({
				      source: newData,
				      select: function (event, ui) {
				        $( "#cod_commerce" ).val(ui.item.data); // display the selected text
					    	$('.create-commerce').addClass('disabled btn-default').removeClass('btn-success').html('Ready!')
					    	$("input[name='title']").val(ui.item.label)
					    },
				      response: function(event, ui) {
				      	if(ui.content.length != 0){
				      		for(i = 0; i < ui.content.length; i++){
						      	//if the typing text is exactly equal to the result no matter the uppercases
										if(ui.content[i].label.toLocaleLowerCase() == $( "#cod_commerce1" ).val().toLocaleLowerCase()){
											$( "#cod_commerce" ).val(ui.content[i].data);
											// console.log('identicos')
											$('.create-commerce').addClass('disabled btn-default').removeClass('btn-success').html('Select it')
										}	      			
										else{
											$( "#cod_commerce" ).val('');
											$('.create-commerce').addClass('btn-success').removeClass('disabled btn-default').html('Create first')
											// console.log('no son iguales')
										}
				      		}
				      	}
				      	else{
			      			// console.log('no coinside')
									$('.create-commerce').addClass('btn-success').removeClass('disabled btn-default').html('Create first')
			      		}
		            if (ui.content.length === 0) {
		            		$( "#cod_commerce" ).val('');
		            		// console.log('nothing')
		            }
		        	}
				    });
		      },
		      error: function () {                
		      }
		    })
	  		
	  	}
	  }
	}

	createCommerce()

	function createCommerce(){
		$('.btn.create-commerce').on('click', function(){

			commerceName= $('#cod_commerce1').val()
			commerceCategory= $('#cod_category option:selected').val()

			datos= {"name":commerceName, "id":commerceCategory}
			datos=JSON.stringify(datos)
			if(commerceName != '' && commerceCategory != ''){
			  $.ajax({
		      url: '/commerce/quickCreate/'+datos+'',
		      type: 'post',
		      success: function (data) {
		      	$('#cod_commerce').val(data)
		      	$('.btn.create-commerce').html('<i class="fa fa-check"></i> Created').addClass('disabled')
		      	getCommerces2()
		      }
		    })
		   }

		})
	}


})