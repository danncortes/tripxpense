$(function(){

	function switcher(){
		switchButt=$('.switch')
		topButton=switchButt.find('.topbuttons .button')
		midButton=switchButt.find('.middlebuttons .button')


		statsDetail= $('.stats-detail')
		statsDetail.hide()
		$('.stats-detail.active').show()
		widthButton=topButton.width()
		marginB=parseInt($('.topbuttons .button:nth-child(1n+2)').css('margin-left'), 10)
		topButton.on('click', function(){
			indexItem=$(this).index();
			topButton.removeClass('active')
			$(this).addClass('active')

			midButton.animate({
				left: (indexItem * widthButton) + (indexItem * marginB)
			}, 200)

			dataType=$(this).attr('data-type')
			statsDetail.hide()
			$('.stats-detail' + '.' + dataType).show()

			$('.stats .titlearea .title').siblings('.type').html(dataType)
		})

	}

    if($('.stats').attr('data-id')=="all"){
        url="/statsOperations";
    }
    else{
        url="/statsOperations/" + $('.stats').attr('data-id');
    }

	$.ajax(
	{
    url: url,
    type: 'POST',
    dataType: 'json',
    error: function(data){
        console.log(data)
    },
    success: function(data) {

    	colors=[]
    	for(i = 0; i < data[0]['categories'].length; i++){
    		colors[0]={'cat':data[0]['categories'][0]['name'], 'value':'#cc6666'}
    		colors[1]={'cat':data[0]['categories'][1]['name'], 'value':'#cc6633'}
    		colors[2]={'cat':data[0]['categories'][2]['name'], 'value':'#cccc66'}
    		colors[3]={'cat':data[0]['categories'][3]['name'], 'value':'#ffcc33'}
    		colors[4]={'cat':data[0]['categories'][4]['name'], 'value':'#99cc66'}
    		colors[5]={'cat':data[0]['categories'][5]['name'], 'value':'#66cccc'}
    		colors[6]={'cat':data[0]['categories'][6]['name'], 'value':'#009999'}
    		colors[7]={'cat':data[0]['categories'][7]['name'], 'value':'#66cc66'}
    		colors[8]={'cat':data[0]['categories'][8]['name'], 'value':'#6699cc'}
    		colors[9]={'cat':data[0]['categories'][9]['name'], 'value':'#6666cc'}
    		colors[10]={'cat':data[0]['categories'][10]['name'], 'value':'#9966cc'}
    		colors[11]={'cat':data[0]['categories'][11]['name'], 'value':'#cc66cc'}
    		colors[12]={'cat':data[0]['categories'][12]['name'], 'value':'#cc6699'}
    	}

    	spent=[]
    	for(i = 0; i < data[0]['categories'].length; i++){
    		cat=data[0]['categories'][i]['name']
    		for(i2 = 0; i2 < colors.length; i2++){
	    		if(colors[i2]['cat']==cat){
	    			theColor=colors[i]['value']
	    		}
	    	}
    		spent[i]={'label':data[0]['categories'][i]['name'], 'value':data[0]['categories'][i]['number'], 'color':theColor}

    		$('.stats .stats-detail.Spent .legend').append('<li>'+'<div class="color" style="background:'+theColor+'"></div>'+'<span> ($ '+data[0]['categories'][i]['number']+')</span> - '+data[0]['categories'][i]['name']+'</li>')
    	}

    	purchases=[]
    	for(i = 0; i < data[1]['categories'].length; i++){
    		cat=data[1]['categories'][i]['name']
    		if(colors[i]['cat']==cat){
    			theColor=colors[i]['value']
    		}
    		purchases[i]={'label':data[1]['categories'][i]['name'], 'value':data[1]['categories'][i]['number'], 'color':theColor}
    		$('.stats .stats-detail.Purchases .legend').append('<li>'+'<div class="color" style="background:'+theColor+'"></div>'+'<span> ('+data[1]['categories'][i]['number']+') </span> - '+data[1]['categories'][i]['name']+'</li>')
    	
    	}

    	percent=[]
    	for(i = 0; i < data[2]['categories'].length; i++){
    		cat=data[2]['categories'][i]['name']
    		if(colors[i]['cat']==cat){
    			theColor=colors[i]['value']
    		}
    		percent[i]={'label':data[2]['categories'][i]['name'], 'value':data[2]['categories'][i]['number'], 'color':theColor}
    		$('.stats .stats-detail.Percent .legend').append('<li>'+'<div class="color" style="background:'+theColor+'"></div>'+'<span> (% '+data[2]['categories'][i]['number']+') </span> - '+data[2]['categories'][i]['name']+'</li>')
    	
    	}

    	console.debug(spent)
    	console.debug(purchases)
    	console.debug(percent)
  	},
  	complete: function(){
  		var ctx = document.getElementById("chart-area-percent").getContext("2d");
			window.myDoughnut = new Chart(ctx).Doughnut(percent, {responsive : true});
  		var ctx2 = document.getElementById("chart-area-spent").getContext("2d");
			window.myDoughnut = new Chart(ctx2).Doughnut(spent, {responsive : true});
  		var ctx3 = document.getElementById("chart-area-purchases").getContext("2d");
			window.myDoughnut = new Chart(ctx3).Doughnut(purchases, {responsive : true});
  		switcher()
  	}
	})
})