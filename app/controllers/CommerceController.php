<?php

class CommerceController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$commerces = Commerce::orderBy('created_at', 'desc')->paginate(15);
		$categories = Category::all();
		return View::make('commerce.index')->with('commerces', $commerces)->with('categories', $categories);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$categories = Category::all();
		return View::make('commerce.create')->with('categories', $categories);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$commerce= new Commerce;
		$returnValidate=$commerce::validateCommerce();

		$cod_category=Input::get('cod_category');
		$newCommerce=Input::get('name');
		$commerces= Commerce::where('cod_category', $cod_category)->where('name', $newCommerce)->get()->count();

		if(! $returnValidate->fails() && $commerces==0)
		{
			$saveCommerce=$commerce::saveCommerce($commerce);
			if($saveCommerce==true){
				return Redirect::to('/commerce');
			}
			else
			{
				return Redirect::route('commerce.create')->withInput()->with('message', 'Error saving data!');
			}
		}
		else
		{
			if($commerces!=0){
				$returnValidate->errors()->add('name', 'This commerce has been already created in this category');
			}
			return Redirect::route('commerce.create')->withInput()->withErrors($returnValidate);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$categories = Category::all();
		$commerce = Commerce::find($id);
		$userL = new User;
		$userLevel=$userL::userLevel();
		// $userLevel=$this::userLevel();

		if($userLevel>0){
			return View::make('commerce.edit')->with('commerce', $commerce)->with('categories', $categories);
		}
		else{
			return Redirect::to('/commerce');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$commerce = Commerce::find($id);
					
		$newCommerce= new Commerce;
		$returnValidate=$newCommerce::validateCommerce($commerce);

		$cod_category=Input::get('cod_category');
		$thisCommerce=Input::get('name');


		if( strcasecmp($thisCommerce, $commerce->name)==0 && $cod_category == $commerce->cod_category)
		{
			$commerces=0;
		}
		else{
			$commerces= Commerce::where('cod_category', $cod_category)->where('name', $thisCommerce)->get()->count();
		}

		if(! $returnValidate->fails() && $commerces==0)
		{	
			$updateCommerce=$newCommerce::saveCommerce($commerce);
			if($updateCommerce==true)
			{
				return Redirect::to('/commerce');
			}
			else{
				return Redirect::route('commerce.edit', $commerce->id)->with('message', 'Error while saving!');
			}
		}
		//Validator fails
		else
		{
			if($commerces!=0){
				$returnValidate->errors()->add('name', 'This commerce has been already created in this category');
			}
			//Return to commerce.edit with message and validation errors
			return Redirect::route('commerce.edit', $commerce->id)->withErrors($returnValidate);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = new User;
		$userLevel=$user::userLevel();
		// $userLevel=$this::userLevel();
		if($userLevel>0)
		{
			$commerce = Commerce::find($id);		

			if($commerce != null)
			{
				try
				{
					$commerce->delete();
					return Redirect::to('/commerce')->with('message', 'Pay Method successfully deleted');
				}
				catch(Exception $e)
				{
					return Redirect::to('/commerce')->with('message', $e->getMessage());
				}
			}
			else
			{
				return Redirect::to('/commerce');
			}
		}
		else{
			return Redirect::to('/commerce');
		}
	}

	public function findCommerces($id){
		$commerces = Commerce::where('cod_category', $id)->get();
		return $commerces;
		// return Redirect::to('operation/create')->with('commerces', $commerces);
	}

	public function quickCreate($datos){
		$commerce= new Commerce;
		
		$datos=json_decode($datos);
		$commerce->cod_category=$datos->{'id'};
		$commerce->name=$datos->{'name'};

		$saveCommerce=$commerce::saveQuickCommerce($commerce);
		if($saveCommerce==true){
			return $commerce->id;
		}
		else
		{
			return 'false';
		}
		// return Redirect::to('operation/create')->with('commerces', $commerces);
	}

}