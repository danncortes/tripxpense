<?php

class Pay_methodController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = new User;
		$userLevel=$user::userLevel(); /*Get the user level, if is admin...*/
		// $userLevel=$this::userLevel();
		if($userLevel>0)
		{
			$pay_methods = Pay_method::all();
			return View::make('pay_method.index')->with('pay_methods', $pay_methods);	
		}
		else{
			return Redirect::to('/dashboard');
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user = new User;
		$userLevel=$user::userLevel();
		// $userLevel=$this::userLevel();
		if($userLevel>0)
		{
			return View::make('pay_method.create');
		}
		else{
			return Redirect::to('/profile');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$pay_method= new Pay_method;
		$returnValidate=$pay_method::validatePay_method();
		if(! $returnValidate->fails())
		{
			$savePay_method=$pay_method::createPay_method($pay_method);
			if($savePay_method==true){
				return Redirect::to('/pay_method');
			}
			else
			{
				return Redirect::route('pay_method.create')->withInput()->with('message', 'Error saving data!');
			}
		}
		else
		{
			return Redirect::route('pay_method.create')->withInput()->withErrors($returnValidate);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$pay_method = Pay_method::find($id);
		$userL = new User;
		$userLevel=$userL::userLevel();
		// $userLevel=$this::userLevel();

		if(Auth::check()){
			if($userLevel>0){
				return View::make('pay_method.edit')->with('pay_method', $pay_method);
			}
			else{
				return Redirect::to('/dashboard');
			}
		}
		else{
			return Redirect::to('/login');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$pay_method = Pay_method::find($id);
					
		$newPay_method= new Pay_method;
		$validator=$newPay_method::validateUpdate($pay_method);

		if(! $validator->fails())
		{	
			$updatePay_method=$newPay_method::updatePay_method($pay_method);
			if($updatePay_method==true)
			{
				return Redirect::to('/pay_method');
			}
			else{
				return Redirect::route('pay_method.edit', $pay_method->id)->with('message', 'Error while saving!');
			}
		}
		//Validator fails
		else
		{
			//Return to Pay_method.edit with message and validation errors
			return Redirect::route('pay_method.edit', $pay_method->id)->withErrors($validator);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = new User;
		$userLevel=$user::userLevel();
		// $userLevel=$this::userLevel();
		if($userLevel>0)
		{
			$pay_method = Pay_method::find($id);		

			if($pay_method != null)
			{
				try
				{
					$pay_method->delete();
					return Redirect::to('/pay_method')->with('message', 'Pay Method successfully deleted');
				}
				catch(Exception $e)
				{
					return Redirect::to('/pay_method')->with('message', $e->getMessage());
				}
			}
			else
			{
				return Redirect::to('/pay_method');
			}
		}
		else{
			return Redirect::to('/profile');
		}
	}

}