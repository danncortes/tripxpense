<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public function __construct(){
		if(Auth::check()){
			$authUser=Auth::user()->id;
			$travels = Travel::where('cod_user', Auth::user()->id)->orderBy('finish', 'DESC')->get();
			View::share('travels', $travels);
		}
	}
}