<?php

class TravelController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$authUser=Auth::user()->id;
		$travels = Travel::where('cod_user', $authUser)->orderBy('finish', 'DESC')->get();
		return View::make('travel.index')->with('travels', $travels);	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// return View::make('travel.create');
		return View::make('travel.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$travel= new Travel;
		$returnValidate=$travel::validateTravel();

		$fechainicio=$travel->start = Input::get('start');
		$fechafin=$travel->finish = Input::get('finish');

		if(! $returnValidate->fails())
		{
			if($fechainicio>$fechafin){
				return Redirect::route('travel.create')->withInput()->with('message', 'The Start date must be less than the finish date for the trip');
			}
			else{
				$saveTravel=$travel::createTravel($travel);
			 	if($saveTravel==true){
			 		return Redirect::to('/travel');
			 	}
			 	else
			 	{
			 		return Redirect::route('travel.create')->withInput()->with('message', 'Error saving data!');
			 	}
			}
		}
		else
		{
			return Redirect::route('travel.create')->withInput()->withErrors($returnValidate);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$travel = new Travel;
		$authUser=Auth::user()->id;
		$travel = Travel::where('cod_user', $authUser)->find($id);

		$operation= new Operation;

		$rangeStat=$id;
		$statsOperations=$operation::statsOperations($rangeStat);

		$scopeBalance=$id;
		$getBalancePerTravel=$operation::getBalancePerTravel($scopeBalance);

		$operations = Operation::where('cod_user', $authUser)->where('cod_travel', $id)->orderBy('id', 'DESC')->get();

		return View::make('travel.show')->with('totalCost', $getBalancePerTravel)->with('statsOpe', $statsOperations)->with('operations', $operations)->with('currTravel', $travel);	
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$AuthUserId=Auth::user()->id;/*Get the Auth user id*/
		$travel = Travel::find($id); /*Get the travel id to edit*/
		$idUserTravel=$travel->cod_user; /*Get from this travel the user id*/
		if($AuthUserId==$idUserTravel){ /*Validate if the User Auth id is the same to the user id of the travel to edit , if yes, could be edited*/
			return View::make('travel.edit')->with('travel', $travel);		
		}
		else{
			return Redirect::route('travel.index');	
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$travel = Travel::find($id);

		$newTravel= new Travel;
		$validator=$newTravel::validateUpdate($travel);

		$fechainicio=$newTravel->start = Input::get('start');
		$fechafin=$newTravel->finish = Input::get('finish');

		if(! $validator->fails())
		{	
			if($fechainicio>$fechafin){
				return Redirect::route('travel.edit', $travel->id)->withInput()->with('message', 'The Start date must be less than the finish date for the trip');
			}
			else{
				$updateTravel=$newTravel::updateTravel($travel);
				if($updateTravel==true)
				{
					return Redirect::to('/travel');
				}
				else{
					return Redirect::route('travel.edit', $travel->id)->with('message', 'Error while saving!');
				}
			}
		}
		//Validator fails
		else
		{
			//Return to user.edit with message and validation errors
			return Redirect::route('travel.edit', $travel->id)->withErrors($validator);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$travel = Travel::find($id);

		if($travel != null)
		{
			$AuthUserId=Auth::user()->id;
			$idUserTravel=$travel->cod_user;
			if($AuthUserId==$idUserTravel){
				try
				{
					$travel->delete();
					return Redirect::to('/travel')->with('message', 'Travel successfully deleted');
				}
				catch(Exception $e)
				{
					return Redirect::to('/travel')->with('message', $e->getMessage());
				}
			}
			else{
				return Redirect::to('/travel');
			}
		}
		else
		{
			return Redirect::to('/travel');
		}	

			
	}

}