<?php

class UserController extends \BaseController {


	/*LOGIN USER*/
	public function login()
	{
		return View::make('user.login');
	}

	/*AUTHENTICATE USER*/
	public function authenticate()
	{
		$rules = array(
			'email' => 'required|email',
			'password' => 'required|min:6'
		);

		$validator = Validator::make(Input::all(), $rules);

		if(! $validator->fails())
		{
			$user = array('email' => Input::get('email'), 'password' => Input::get('password'));
			if(Auth::attempt($user)) /*intentar autenticar usuario, si los datos existen el usuario es validado*/
			{
				return Redirect::to('/dashboard');
			}
			else
			{
				return Redirect::to('/login')->with('message', 'Error on email or password');
			}
		}
		else{
			return Redirect::to('/login')->withInput()->withErrors($validator);
		}
	}

	// public function userLevel(){
	// 	if(Auth::check()){
	// 		$userLevel=Auth::user()->level;
	// 		return $userLevel;
	// 	}
	// }


	/*LOGOUT USER*/
	public function logout(){
		Auth::logout();
		return Redirect::to('/login');
	}

	public function dashboard(){
		$authUser=Auth::user()->id;
		$travels = Travel::where('cod_user', $authUser)->get();

		$operation= new Operation;
		// Getting stats in percent
		$rangeStat='all';
		$statsOperations=$operation::statsOperations($rangeStat);
		// End getting stats in percent


		// Getting balance per travel
		$scopeBalance='all';
		$getBalancePerTravel=$operation::getBalancePerTravel($scopeBalance);
		// End getting balance per travel


		$operations = Operation::where('cod_user', $authUser)->orderBy('id', 'DESC')->get();
		// return $operations;
		return View::make('user.dashboard')->with('travels', $travels)->with('totalCost', $getBalancePerTravel)->with('operations', $operations);
		// return $getBalancePerTravel;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = new User;
		$userLevel=$user::userLevel();
		// $userLevel=$this::userLevel();
		if($userLevel>0)
		{
			$users = User::all();
			return View::make('user.index')->with('usuarios', $users);	
		}
		else{
			return Redirect::to('/dashboard');
		}
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user = new User;
		$userLevel=$user::userLevel();
		// $userLevel=$this::userLevel();
		if(!Auth::check() || $userLevel>0)
		{
			return View::make('user.create');
		}
		else{
			return Redirect::to('/profile');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$user= new User;
		$returnValidate=$user::validateUser();
		if(! $returnValidate->fails())
		{
			$saveUser=$user::createUser($user);
			if($saveUser==true){
				
				// sending a welcome email
				Mail::send('emails.welcome', array('key' => Input::get('name')), function($message)
				{
				    $message->to(Input::get('email'), Input::get('name'))->subject('Welcome!');
				});

				return Redirect::to('/dashboard');

			}
			else
			{
				return Redirect::route('user.create')->withInput()->with('message', 'Error saving data!');
			}
		}
		else
		{
			return Redirect::route('user.create')->withInput()->withErrors($returnValidate);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function showAuthUser(){
		$userId = Auth::user()->id;
		// $this::show($userId);
		$user = User::find($userId);
		return View::make('user.profile')->with('usuario', $user);
	}

	public function show($id)
	{
		$user = new User;
		$userLevel=$user::userLevel();
		// $userLevel=$this::userLevel();
		if($userLevel>0){
			$user = User::find($id);
			return View::make('user.profile')->with('usuario', $user);	
		}
		else{
			return Redirect::to('/profile');
		}

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		$userL = new User;
		$userLevel=$userL::userLevel();
		// $userLevel=$this::userLevel();

		if(Auth::check()){
			if($id==Auth::user()->id){
				return View::make('user.edit')->with('user', $user);
			}
			elseif($userLevel>0 && $id!=Auth::user()->id && $user->level<1){
				return View::make('user.editlevel')->with('user', $user);
			}
			else{
				return Redirect::to('/user')->with('message', 'You can not edit this user!');;
			}

		}
		else{
			return Redirect::to('/login');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = User::find($id);

		if(Hash::check(Input::get('current_password'), $user->password))
		{				
			$newUser= new User;
			$validator=$newUser::validateUpdate($user);

			if(! $validator->fails())
			{	
				$updateUser=$newUser::updateUser($user);
				if($updateUser==true)
				{
					return Redirect::to('/profile');
				}
				else{
					return Redirect::route('user.edit', $user->id)->with('message', 'Error while saving!');
				}
			}
			//Validator fails
			else
			{
				//Return to user.edit with message and validation errors
				return Redirect::route('user.edit', $user->id)->withErrors($validator);
			}
		}
		//Wrong Password
		else
		{
			//Return to user.edit, with message
			return Redirect::route('user.edit', $user->id)->with('message', 'Error in password!');
		}
	}

	/*UPDATE USER LEVEL*/
	public function updatelevel($id)
	{
		$user = User::find($id);

		$newUser= new User;
		$updateUserLevel=$newUser::updateUserLevel($user);
		if($updateUserLevel==true)
		{
			return Redirect::to('/user');
		}
		else{
			return Redirect::route('user.updatelevel', $user->id)->with('message', 'Error while saving!');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);		

		if($user != null)
		{

			try
			{
				$user->delete();
				return Redirect::to('/user')->with('message', 'User successfully deleted');
			}
			catch(Exception $e)
			{
				return Redirect::to('/user')->with('message', $e->getMessage());
			}
		}
		else
		{
			return Redirect::to('/user');
		}
	}

	/*LOGIN USER*/
	public function testing()
	{
		
		// return View::make('testing.test')->with('message', 'esta vaina' );
	}

	public function stats()
	{
		$operation= new Operation;
		
		$rangeStat='all';
		$getBalancePerTravel=$operation::statsOperations($rangeStat);

		return $getBalancePerTravel;

	}



}