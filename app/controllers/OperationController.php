<?php

class OperationController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sort = (Input::get('sort') == '' ? 'date_op' : Input::get('sort'));
		$order = (Input::get('order') == '' ? 'desc' : Input::get('order'));
		$authUser=Auth::user()->id;
		$operations = Operation::where('cod_user', $authUser)->orderBy($sort, $order)->get();
		return View::make('operation.index')->with('operations', $operations)->with('sort', $sort)->with('order', $order);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$start_category= Input::get('category');
		$start_pay_method= Input::get('pay_methods');
		$start_travel= Input::get('travel');

		$pay_methods = Pay_method::all();
		$authUser=Auth::user()->id;
		$categories = Category::orderBy('sort', 'ASC')->get();
		$travels = Travel::where('cod_user', $authUser)->orderBy('finish', 'DESC')->get();
		$country = Country::all();
		return View::make('operation.create')->with('start_travel', $start_travel)->with('start_pay_method', $start_pay_method)->with('start_category', $start_category)->with('pay_methods', $pay_methods)->with('travels', $travels)->with('categories', $categories)->with('country', $country);
		// $pay_methods = Pay_method::all();
		// $authUser=Auth::user()->id;
		// $categories = Category::all();
		// $travels = Travel::where('cod_user', $authUser)->get();
		// $country = Country::all();
		// return View::make('operation.create')->with('pay_methods', $pay_methods)->with('travels', $travels)->with('categories', $categories)->with('country', $country);
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$operation= new Operation;
		$returnValidate=$operation::validateOperation();

		if(! $returnValidate->fails())
		{
			$saveOperation=$operation::createOperation($operation);
		 	if($saveOperation==true){
		 		return Redirect::to('/operation');
		 	}
		 	else
		 	{
		 		return Redirect::route('operation.create')->withInput()->with('message', 'Error saving data!');
		 	}
		}
		else
		{
			return Redirect::route('operation.create')->withInput()->withErrors($returnValidate);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$pay_methods = Pay_method::all();
		$categories = Category::all();
		$country = Country::all();

		$operation = Operation::find($id);

		$commerce=Commerce::find($operation->cod_commerce);
		$commerce_cat=$commerce->cod_category;
		$commerces_ofcat=Commerce::where('cod_category', $commerce_cat)->get();

		if($operation->cod_city!=0){
			$city=City::find($operation->cod_city);
			$cod_country=$city->CountryCode;
			$cities=City::where('CountryCode', $cod_country)->get();
		}
		else{
			$cod_country=[];
			$cities=[];
		}

		$AuthUserId=Auth::user()->id;
		$idUserOperation=$operation->cod_user;

		$travels = Travel::where('cod_user', $AuthUserId)->get();

		if($AuthUserId==$idUserOperation){
			return View::make('operation.edit')
			->with('operation', $operation)
			->with('pay_methods', $pay_methods)
			->with('categories', $categories)
			->with('commerce_cat', $commerce_cat)
			->with('commerces_ofcat', $commerces_ofcat)
			->with('cod_country', $cod_country)
			->with('country', $country)
			->with('cities', $cities)
			->with('travels', $travels);		
		}
		else{
			return Redirect::route('operation.index');	
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$currentOperation = Operation::find($id);
					
		$operation= new Operation;
		$validator=$operation::validateOperation($currentOperation);

		if(! $validator->fails())
		{	
			$updateOperation=$operation::createOperation($currentOperation);
			if($updateOperation==true)
			{
				return Redirect::to('/operation');
			}
			else{
				return Redirect::route('operation.edit', $currentOperation->id)->with('message', 'Error while saving!');
			}
		}
		//Validator fails
		else
		{
			return Redirect::route('operation.edit', $currentOperation->id)->withErrors($validator);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$operation = Operation::find($id);
		if($operation != null)
		{
			$AuthUserId=Auth::user()->id;
			$idUserOperation=$operation->cod_user;

			if($AuthUserId==$idUserOperation){
				try
				{
					$operation->delete();
					return Redirect::to('/operation')->with('message', 'Operation successfully deleted');
				}
				catch(Exception $e)
				{
					return Redirect::to('/operation')->with('message', $e->getMessage());
				}
			}
			else{
				return Redirect::to('/operation');
			}
		}
		else
		{
			return Redirect::to('/operation');
		}
	}

	public function findOperations()
	{
		$val =  Input::get('finder');
		$authUser=Auth::user()->id;
		if($val!=''){
			$operations = DB::table('operations')
				->where(function($query) use ($authUser, $val){
					$query->where('cod_user', $authUser)
								->where('title', 'LIKE', '%'.$val.'%')
								->orWhere('description', 'LIKE', '%'.$val.'%');
				})
				->get();
			return View::make('operation.results')->with('operations', $operations);
		}
		else{
			return Redirect::to('/operation');
		}
	}

	public function findOperationsTravel($id)
	{
		$val =  Input::get('finder');
		$authUser=Auth::user()->id;
		$travel=Travel::find($id);
		if($val!=''){
			$operations = DB::table('operations')
				->where(function($query) use ($authUser, $val, $id){
					$query->where('cod_user', $authUser)
								->where('cod_travel', $id)
								->where('title', 'LIKE', '%'.$val.'%')
								->orWhere('description', 'LIKE', '%'.$val.'%');
				})
				->get();
			return View::make('operation.results')->with('operations', $operations)->with('travel', $travel)->with('singleTravel', true);
		}
		else{
			return Redirect::to('/operationsTravel/'.$id);
		}
	}

	public function operationsTravel($id)
	{
		$sort = (Input::get('sort') == '' ? 'date_op' : Input::get('sort'));
		$order = (Input::get('order') == '' ? 'desc' : Input::get('order'));
		$authUser=Auth::user()->id;
		$operations = Operation::where('cod_user', $authUser)->where('cod_travel', $id)->orderBy($sort, $order)->get();
		$travel=Travel::where('cod_user', $authUser)->find($id);
		return View::make('operation.index')->with('operations', $operations)->with('sort', $sort)->with('order', $order)->with('travel', $travel)->with('singleTravel', true);
	}

	public function startingOperation(){
		$categories = Category::orderBy('sort', 'ASC')->get();
		$payMethods = Pay_method::all();
		$authUser=Auth::user()->id;
		$travels = Travel::where('cod_user', $authUser)->orderBy('finish', 'DESC')->get();

		$dataStartOperation=array(
			"categories" => $categories,
			"pay_methods" => $payMethods,
			"travels" => $travels,
			"url" => URL::asset("/")
		);
		return $dataStartOperation;
	}

		public function statsOperations2(){
			$operation= new Operation;
			$rangeStat='all';
			$statsOperations=$operation::statsOperations2($rangeStat);
			return $statsOperations;
		}
		public function statsOperations_travel($id){
			$operation= new Operation;
			$statsOperations=$operation::statsOperations2($id);
			return $statsOperations;
		}

}