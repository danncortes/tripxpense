<?php

class CategoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = new User;
		$userLevel=$user::userLevel(); /*Get the user level, if is admin...*/
		if($userLevel>0)
		{
			$category = Category::orderBy('sort', 'ASC')->paginate(15);
			return View::make('category.index')->with('categories', $category);
		}
		else{
			return Redirect::to('/dashboard');
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user = new User;
		$userLevel=$user::userLevel();
		$category = Category::orderBy('sort', 'ASC')->get();
		if($userLevel>0)
		{
			return View::make('category.create')->with('categories', $category);
		}
		else{
			return Redirect::to('/profile');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$category= new Category;
		$returnValidate=$category::validateCategory();
		if(! $returnValidate->fails())
		{
			$saveCategory=$category::createCategory($category);
			if($saveCategory==true){
				return Redirect::to('/category');
			}
			else
			{
				return Redirect::route('category.create')->withInput()->with('message', 'Error saving data!');
			}
		}
		else
		{
			return Redirect::route('category.create')->withInput()->withErrors($returnValidate);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = Category::find($id);
		$categories = Category::orderBy('sort', 'ASC')->get();
		$userL = new User;
		$userLevel=$userL::userLevel();
		// $userLevel=$this::userLevel();

		if($userLevel>0){
			return View::make('category.edit')->with('category', $category)->with('categories', $categories);
		}
		else{
			return Redirect::to('/dashboard');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$category = Category::find($id);
					
		$newCategory= new Category;
		$validator=$newCategory::validateUpdate($category);

		if(! $validator->fails())
		{	
			$updateCategory=$newCategory::updateCategory($category);
			if($updateCategory==true)
			{
				return Redirect::to('/category');
			}
			else{
				return Redirect::route('category.edit', $category->id)->with('message', 'Error while saving!');
			}
		}
		//Validator fails
		else
		{
			//Return to category.edit with message and validation errors
			return Redirect::route('category.edit', $category->id)->withErrors($validator);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = new User;
		$userLevel=$user::userLevel();
		if($userLevel>0)
		{
			$category = Category::find($id);		

			if($category != null)
			{
				try
				{
					File::delete(public_path()."/".$category->pic);
					$category->delete();
					return Redirect::to('/category')->with('message', 'Category successfully deleted');
				}
				catch(Exception $e)
				{
					return Redirect::to('/category')->with('message', $e->getMessage());
				}
			}
			else
			{
				return Redirect::to('/category');
			}
		}
		else{
			return Redirect::to('/dashboard');
		}
	}
	
	public function removePic($id){
		$category = Category::find($id);
		$newCategory= new Category;
		$removePicCategory=$newCategory::removePicCategory($category);
	}
}