@extends('layouts.base')

@section('titulo')
	Users - 
@stop
@section('contenido')
	<h3>Users</h3><br/>
	@if(Session::has('message'))
		<p class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{{Session::get('message')}}
		</p>
	@endif
	<a href="{{URL::asset('/user/create')}}" class="btn btn-success">Register New User</a><br/><br/>
	<table class="table table-bordered">
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th></th>
		</tr>
	@foreach($usuarios as $usuario)
		<tr>
			<td>
				{{ $usuario->name }}
			</td>
			<td>
				{{ $usuario->email }}
			</td>
			<td>
<!-- 				{{Form::open(array('url' => 'user'. '/' . $usuario->id,'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this user?")', 'class' => 'pull-left'))}}
					{{Form::button("<i class='fa fa-pencil'></i>" , array('class' => 'btn btn-primary btn-xs', 'type' => 'submit'))}}
				{{Form::close()}} -->
				<a href="{{URL::asset("/user") .'/'. $usuario->id}}/edit" class="btn btn-primary btn-xs pull-left"><i class="fa fa-pencil"></i></a>

				{{Form::open(array('url' => 'user'. '/' . $usuario->id,'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this user?")', 'class' => 'pull-left'))}}
					{{Form::button("<i class='fa fa-trash-o'></i>" , array('class' => 'btn btn-danger btn-xs', 'type' => 'submit'))}}
				{{Form::close()}}
			</td>
		</tr>
	@endforeach
	</table>
@stop