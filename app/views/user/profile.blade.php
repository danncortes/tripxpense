@extends('layouts.base')

@section('titulo')
	Profile - 
@stop
@section('contenido')
	<div class="row txtcenter">
		<div class="col-xs-6 col-xs-offset-3">
			<div class="well">
				<div class="panel panel-primary margin-b-0">
					<div class="panel-heading">
						<h3 class="panel-title">Profile User <a href='{{URL::asset("/user") .'/'. Auth::user()->id}}/edit' class="btn btn-default btn-xs pull-right"><i class="fa fa-pencil"></i> Edit Profile</a><div class="clear"></div></h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-4 txt-center">
								<i class="fa fa-user fa-5x"></i>
							</div>
							<div class="col-xs-8">
								<h4>Nombre</h4>
								<p>{{$usuario->name}}</p>
								<hr>
								<h4>Email</h4>
								<p>{{$usuario->email}}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop