@extends('layouts.base')

@section('titulo')
	Login - 
@stop
@section('contenido')
	<div class="row txtcenter">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3">
			<div class="well">
				<h2 class="margin-t-0">Login</h2>
				<hr/>

				@if(Session::has('message'))
			  		<p class="alert alert-danger">{{session::get('message')}}</p>
			  	@endif

				{{Form::open(array('url' => '/authenticate','method' => 'post', 'role' => 'form'))}}
					<fieldset class="form-group">
						{{Form::label('email', 'Email: ')}}
						{{Form::text('email', Input::old('email'), array('class' => 'form-control', 'required'))}} <!--el old trae el valor de vuelta cuando se genera error, el dato se guarda en la variable de sesion y cuando te regresa al forma para montrar el error te coloca la info que ya habias puesto enel form -->
					</fieldset>
					@if($errors->has('email'))
						{{Form::label('email', $errors->first('email'), array('class' => 'alert alert-danger'))}}
					@endif

					<fieldset class="form-group">
						{{Form::label('password', 'Password: ')}}
						{{Form::password('password', array('class' => 'form-control', 'required'))}}
					</fieldset>
					@if($errors->has('password'))
						{{Form::label('password', $errors->first('password'), array('class' => 'alert alert-danger'))}}
					@endif

					{{Form::submit('Login', array('class' => 'btn btn-warning btn-block'))}}
					<br>
					<br>
					<a href="/password/remind">Forgot Password?</a>
				{{Form::close()}}
				<hr>
				<p class="txt-center">Without an account?</p>
				<a href="{{URL::asset('/user/create')}}" class="btn btn-success btn-block">Sign Up</a>
			</div>
		</div>
	</div>
@stop