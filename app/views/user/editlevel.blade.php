@extends('layouts.base')

@section('titulo')
	Edit Level User - 
@stop
@section('contenido')
	<div class="row">
		<div class="col-xs-6 col-xs-offset-3">
			<div class="well">
				<h2 class="margin-t-0">Edit Level User</h2>
				<hr>
				@if(Session::has('message'))
					<p class="alert alert-danger">{{Session::get('message')}}</p>
				@endif
				{{Form::open(array('route' => array('user.updatelevel', $user->id), 'method' => 'POST'))}}
					<fieldset class="form-group">
						{{Form::label('Level', 'Change User Level for: ' . $user->name)}}
						<div class="radio">
						  <label>
						  	{{ Form::radio('level', '0', true) }}
						    Basic
						  </label>
						</div>
						<div class="radio">
						  <label>
						  	{{ Form::radio('level', '1') }}
						    Admin
						  </label>
						</div>
					</fieldset>
					{{Form::submit('Save', array('class' => 'btn btn-primary'))}}
					{{Form::close()}}
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop