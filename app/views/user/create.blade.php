@extends('layouts.base')

@section('titulo')
	Sign Up - 
@stop
@section('contenido')
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3">
			<div class="well">
				<h2 class="margin-t-0">Sign Up</h2>
				<hr>
				@if(Session::has('message'))
					<p class="alert alert-danger">{{Session::get('message')}}</p>
				@endif
				{{Form::open(array('route' => 'user.store','method' => 'post'))}}
					<fieldset class="form-group">
						{{Form::label('name', 'Name: ')}}
						{{Form::text('name', Input::old('name'), array('class' => 'form-control', 'required'))}}
					</fieldset>
					@if($errors->has('name'))
						{{Form::label('name', $errors->first('name'), array('class' => 'alert alert-danger'))}}
					@endif

					<fieldset class="form-group">
						{{Form::label('email', 'Email: ')}}
						{{Form::text('email', Input::old('email'), array('class' => 'form-control', 'required'))}}
					</fieldset>
					@if($errors->has('email'))
						{{Form::label('email', $errors->first('email'), array('class' => 'alert alert-danger'))}}
					@endif

					<fieldset class="form-group">
						{{Form::label('password', 'Password: ')}}
						{{Form::password('password', array('class' => 'form-control'))}}
					</fieldset>
					@if($errors->has('password'))
						{{Form::label('password', $errors->first('password'), array('class' => 'alert alert-danger'))}}
					@endif
					<fieldset class="form-group">
						{{Form::label('password_confirmation', 'Confirm Password: ')}}
						{{Form::password('password_confirmation', array('class' => 'form-control'))}}
					</fieldset>
					@if($errors->has('password_confirmation'))
						{{Form::label('password_confirmation', $errors->first('password_confirmation'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif
					@if(Auth::check() && Auth::user()->level>0)
						<fieldset class="form-group">
							{{Form::label('Level', 'User Level: ')}}
							<div class="radio">
							  <label>
							  	{{ Form::radio('level', '0', true) }}
							    Basic
							  </label>
							</div>
							<div class="radio">
							  <label>
							  	{{ Form::radio('level', '1') }}
							    Admin
							  </label>
							</div>
						</fieldset>
					@endif
					{{Form::submit('Sign Up', array('class' => 'btn btn-warning btn-block'))}}
					{{Form::close()}}
				{{Form::close()}}
				<hr>
				<p class="txt-center">Already with account?</p>
				<a href="{{URL::asset('/login')}}" class="btn btn-success btn-block">Login</a>
			</div>
		</div>
	</div>
@stop