@extends('layouts.base')

@section('titulo')
	Edit User - 
@stop
@section('contenido')
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3">
			<div class="well">
				<h2 class="margin-t-0">Edit User</h2>
				<hr>
				@if(Session::has('message'))
					<p class="alert alert-danger">{{Session::get('message')}}</p>
				@endif
				{{Form::open(array('route' => array('user.update', $user->id), 'method' => 'PUT'))}}
					<fieldset class="form-group">
						{{Form::label('name', 'Name: ')}}
						{{Form::text('name', $user->name, array('class' => 'form-control', 'required'))}}
					</fieldset>
					@if($errors->has('name'))
						{{Form::label('name', $errors->first('name'), array('class' => 'alert alert-danger'))}}
					@endif

					<fieldset class="form-group">
						{{Form::label('email', 'Email: ')}}
						{{Form::text('email', $user->email, array('class' => 'form-control', 'required'))}}
					</fieldset>
					@if($errors->has('email'))
						{{Form::label('email', $errors->first('email'), array('class' => 'alert alert-danger'))}}
					@endif

					<fieldset class="form-group">
						{{Form::label('new_password', 'New password: ')}}
						{{Form::password('new_password', array('class' => 'form-control'))}}
					</fieldset>
					@if($errors->has('new_password'))
						{{Form::label('new_password', $errors->first('new_password'), array('class' => 'alert alert-danger'))}}
					@endif

					<fieldset class="form-group">
						{{Form::label('new_password_confirmation', 'Confirm New Password: ')}}
						{{Form::password('new_password_confirmation', array('class' => 'form-control'))}}
					</fieldset>
					@if($errors->has('new_password_confirmation'))
						{{Form::label('new_password_confirmation', $errors->first('new_password_confirmation'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif

					<fieldset class="form-group">
						{{Form::label('current_password', 'For update your profile, please insert the current password')}}
						{{Form::password('current_password', array('class' => 'form-control'))}}
					</fieldset>

					@if(Auth::check() && Auth::user()->level>0)
						<fieldset class="form-group">
							{{Form::label('Level', 'User Level: ')}}
							<div class="radio">
							  <label>
							  	{{ Form::radio('level', '0', true) }}
							    Basic
							  </label>
							</div>
							<div class="radio">
							  <label>
							  	{{ Form::radio('level', '1') }}
							    Admin
							  </label>
							</div>
						</fieldset>
					@endif
					{{Form::submit('Save', array('class' => 'btn btn-success btn-block'))}}
					{{Form::close()}}
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop