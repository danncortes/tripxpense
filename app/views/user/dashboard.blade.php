@extends('layouts.base')

@section('titulo')
	Dashboard - 
@stop
@section('contenido')
	<div class="row">
		<div class="col-xs-12 col-md-8">
			<div class="well">
				<h2 class="margin-t-0">My Balance</h2>
				<hr>

					<!-- Wrapper for slides -->
					@if (empty($totalCost))

						<div class="txt-center">
							There are no expenses yet!
						</div>
						
					@else
						<div id="carousel-balance" class="balance-dashboard carousel slide" data-ride="carousel">
							
							<?php $ind=0;?>
							<ol class="carousel-indicators">
								@foreach($totalCost as $travel)
									@if($ind==0)
										<?php $state='active';?>
									@else
										<?php $state='';?>
									@endif
									{{'<li data-target="#carousel-balance" data-slide-to="'.$ind.'" class="'.$state.'"></li>'}}
								<?php $ind++;?>
								@endforeach
							</ol>
		
							<div class="carousel-inner">
								<?php $i=0;?>
								@foreach($totalCost as $travel)
									@if($i==0)
										{{'<div class="item active">'}}
										<?php $i=1; ?>
									@else
										{{'<div class="item">'}}
									@endif
										<div class="cont-balance">
											<a href="/travel/{{$travel['id']}}">
												<h1 class="title-ppal"> <i class="fa fa-plane"></i>{{ $travel['name']}}</h1>
												
												@foreach($travel['paymethods'] as $method)
													<div class="method-area">

														<div class="title-area">
															<div class="initial">
																Initial: ${{$method['initial']}}
															</div>
															<h4 class="title">
																@if($method['name']=='cash')
																	<i class="fa fa-money success"></i>
																@elseif($method['name']=='credit card')
																	<i class="fa fa-credit-card warning"></i> 
																@else
																	<i class="fa fa-credit-card primary"></i>
																@endif
																{{$method['name']}}
															</h4>
														</div>

														<div class="progress">

															@if($method['percent_remaining']<=0)
																<?php $perc_remaining=0; ?> 
															@else
																<?php $perc_remaining=$method['percent_remaining']; ?> 
															@endif
														  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{$perc_remaining}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$perc_remaining}}%;" title="Remaining = ${{$method['remaining']}} / {{$method['percent_remaining']}}%" data-toggle="tooltip" data-placement="top">
														    Remaining = ${{$method['remaining']}} / {{$method['percent_remaining']}}%
														  </div>
														  @if($method['remaining']<=0)
														  	<?php $percent_spent=100; ?> 
														  	@if($method['initial']<=0)
															  	<div class="progress-bar progress-bar-danger" style="width: {{$percent_spent}}%">
															    No Budget! - ${{$method['spent']}} / {{$method['percent_spent']}}%
															   @else
															  	<div class="progress-bar progress-bar-danger" style="width: {{$percent_spent}}%">
															    Budget Ended! - ${{$method['spent']}} / {{$method['percent_spent']}}%
															   @endif
														  @else
														  	<div class="progress-bar" style="width: {{$method['percent_spent']}}%" title="Spent = ${{$method['spent']}} / {{$method['percent_spent']}}%" data-toggle="tooltip" data-placement="top">
														    Spent = ${{$method['spent']}} / {{$method['percent_spent']}}%
														  @endif
														  </div>

														</div>

													</div>
												@endforeach
											</a>
										</div>
									</div>
								@endforeach
							</div>

						</div>
					@endif
			</div>
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="well">
				<h2 class="margin-t-0">Latest operations</h2>
				<hr>
				@if (count($operations) == 0)
					{{"There are no operations yet"}}
				@else
					@foreach(array_slice($operations->toArray(), 0, 3) as $operation)
						<div class="btn btn-sm btn-block ope-box">
							<div class="row ">
								<div class="col-xs-8 title">
									{{$operation['title']}}
								</div>
								<div class="col-xs-4 date">
									{{$operation['date_op']}}
								</div>
							</div>

							<div class="cost">
								<h2 class="mount">
									{{'<span>$ </span>'.$operation['cost']}}
									@if($operation['cod_method']==1)
										<i class="icon fa fa-credit-card warning"></i>
									@elseif($operation['cod_method']==2)
										<i class="icon fa fa-credit-card primary"></i>
									@else
										<i class="icon fa fa-money success"></i>
									@endif
								</h2>
							</div>
							<?php $thisTravel = Travel::find($operation['cod_travel']);?>
							<p class="travel">{{$thisTravel->name}}</p>
						</div>
					@endforeach
					<a href="/operation" class="btn btn-xs btn-block">See all</a>
				@endif
			</div>
			
		</div>
	</div>
	<div class="well stats" data-id="all">
		<div class="titlearea">
			<h2 class="title">Stats</h2>
			<div class="switch">

				<div class="topbuttons">
					<div class="button active" data-type="Percent">
						<span>%</span>
					</div>
					<div class="button" data-type="Spent">
						<span>$</span>
					</div>
					<div class="button" data-type="Purchases">
						<span><i class="fa fa-shopping-cart"></i></span>
					</div>
				</div>

				<div class="middlebuttons">
					<div class="button">
					</div>
				</div>
				<div class="backbuttons">
					<div class="button">
					</div>
					<div class="button">
					</div>
					<div class="button">
					</div>
				</div>
			</div>
			<div class="type">Percent</div>
			<div class="clear"></div>
		</div>
		<hr>

		

		<div class="row stats-detail Percent active">
			<div class="col-xs-12 col-sm-4 col-sm-offset-1">
				<div id="canvas-holder" style="width:100%;">
					<canvas id="chart-area-percent" width="100%" height="100%"/>
				</div>
			</div>
			<div class="col-xs-12 col-sm-5 col-sm-offset-2">
				<ul class="legend">
					
				</ul>
			</div>
		</div>

		<div class="row stats-detail Spent">

			<div class="col-xs-12 col-sm-4 col-sm-offset-1">
				<div id="canvas-holder" style="width:100%;">
					<canvas id="chart-area-spent" width="100%" height="100%"/>
				</div>
			</div>
			<div class="col-xs-12 col-sm-5 col-sm-offset-2">
				<ul class="legend">
					
				</ul>
			</div>
		</div>

		<div class="row stats-detail Purchases">

		<div class="col-xs-12 col-sm-4 col-sm-offset-1">
				<div id="canvas-holder" style="width:100%;">
					<canvas id="chart-area-purchases" width="100%" height="100%"/>
				</div>
			</div>
			<div class="col-xs-12 col-sm-5 col-sm-offset-2">
				<ul class="legend">
					
				</ul>
			</div>
		</div>
	</div>
@stop
@section('libs')
	{{ HTML::script('/assets/js/dashboard/dashboard_list_view.js'); }}
	{{ HTML::script('/assets/chartsjs/Chart.Core.js'); }}
	{{ HTML::script('/assets/chartsjs/Chart.Doughnut.js'); }}
@stop