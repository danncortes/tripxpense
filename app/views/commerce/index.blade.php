@extends('layouts.base')

@section('titulo')
	Commerces - 
@stop
@section('contenido')
	<h3>Registered Commerces</h3><br/>
	@if(Session::has('message'))
		<p class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{{Session::get('message')}}
		</p>
	@endif
	<a href="{{URL::asset('/commerce/create')}}" class="btn btn-success">Register New Commerce</a><br/><br/>
	<?php
		$numberCommerce=count($commerces);
	?>
	@if($numberCommerce==0)
		<div class="alert alert-warning">At present, There are not any commerce</div>
	@else
		<table class="table table-bordered">
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Category</th>
				@if(Auth::user()->level>0)
					<th></th>
				@endif
			</tr>
			@foreach($commerces as $commerce)
				<tr>
					<td>
						{{ $commerce->id }}
					</td>
					<td>
						{{ $commerce->name }}
					</td>
					<td>
						<?php
						$idcat=$commerce->cod_category;
						$cat_name = Category::find($idcat);
						?>
						{{ $cat_name->name }}
					</td>
					@if(Auth::user()->level>0)
						<td>
			<!-- 				{{Form::open(array('url' => 'commerce'. '/' . $commerce->id,'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this commerce?")', 'class' => 'pull-left'))}}
								{{Form::button("<i class='fa fa-pencil'></i>" , array('class' => 'btn btn-primary btn-xs', 'type' => 'submit'))}}
							{{Form::close()}} -->
							<a href="{{URL::asset("/commerce") .'/'. $commerce->id}}/edit" class="btn btn-primary btn-xs pull-left"><i class="fa fa-pencil"></i></a>

							{{Form::open(array('url' => 'commerce'. '/' . $commerce->id,'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this commerce?")', 'class' => 'pull-left'))}}
								{{Form::button("<i class='fa fa-trash-o'></i>" , array('class' => 'btn btn-danger btn-xs', 'type' => 'submit'))}}
							{{Form::close()}}
						</td>
					@endif
				</tr>
			@endforeach
		</table>
		<div class="txt-center">{{ $commerces->links() }}</div>
	@endif
@stop