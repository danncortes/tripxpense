@extends('layouts.base')

@section('titulo')
	Edit Commerce - 
@stop
@section('contenido')
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3">
			<div class="well">
				<h2 class="margin-t-0">Edit Commerce</h2>
				<hr>
				@if(Session::has('message'))
					<p class="alert alert-danger">{{Session::get('message')}}</p>
				@endif
				{{Form::open(array('route' => array('commerce.update', $commerce->id), 'method' => 'PUT'))}}
					<fieldset class="form-group">
						{{Form::label('name', 'Name: ')}}
						{{Form::text('name', $commerce->name, array('class' => 'form-control', 'required'))}}
					</fieldset>
					@if($errors->has('name'))
						{{Form::label('name', $errors->first('name'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif
					<fieldset class="form-group">
						{{Form::label('category', 'Category:')}}

						<select id="cod_category" name="cod_category">
							<option>Select</option>
						  @foreach($categories as $category)
						  	@if($commerce->cod_category == $category->id)
									<?php $selected='selected'; ?>
								@else
									<?php $selected=''; ?>
						  	@endif
								{{'<option value="'.$category->id.'" '.$selected.'>'.$category->name.'</option>'}}
							@endforeach
						</select>
					</fieldset>
					@if($errors->has('cod_category'))
						{{Form::label('cod_category', $errors->first('cod_category'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif
					{{Form::submit('Save', array('class' => 'btn btn-success btn-block'))}}
					{{Form::close()}}
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop