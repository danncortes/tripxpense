@extends('layouts.base-mail')
@section('contenido')

		  	<h2 style="color:DodgerBlue; font-weight:100; font-family:helvetica, sans-serif, arial;">
		  		Welcome! {{$key}}
		  	</h2>
			<p style="font-weight:100; color:#666; font-size:1rem; font-family:helvetica, sans-serif, arial;">
		    	We are glad you become member!
		    </p>
		    <table width="100%" cellpadding="0">
		    	<tr>
		    		<td>
			    		<a href="http://tripxpense.com/travel/create">
			    			<table width="200px" cellpadding="12" cellspacing="0" bgcolor="#5cb85c">
			    				<tr>
			    					<td align="center">
			    						<h2 style="margin:0; color:white;">CREATE A TRIP</h2>
			    					</td>
			    				</tr>
			    			</table>
			    		</a>
		    		</td>
		    	</tr>
		    </table>
			<p style="font-weight:100; color:#666; font-size:1rem; font-family:helvetica, sans-serif, arial;">
		    	and start to take the control of your expenses!
		    </p>
@stop