@extends('layouts.base-mail')
@section('contenido')
  <h2 style="color:DodgerBlue; font-weight:100; font-family:helvetica, sans-serif, arial;">Password Reset</h2>
  <p style="font-weight:100; color:#666; font-size:1rem; font-family:helvetica, sans-serif, arial;">
    To reset your password, complete this form:
    <br/>
    {{ URL::to('password/reset', array($token)) }}.
  </p>
  <p style="color:#666;">Thank you for using <a href="http://tripxpense.com" target="blank">Tripxpense.com</a></p>
@stop
