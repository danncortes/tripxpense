@extends('layouts.base')

@section('titulo')
	Pay Methods - 
@stop
@section('contenido')
	<h3>Payment Method</h3><br/>
	@if(Session::has('message'))
		<p class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{{Session::get('message')}}
		</p>
	@endif
	<a href="{{URL::asset('pay_method/create')}}" class="btn btn-success">Create a New Payment Method</a><br/><br/>
	<?php
		$numberPay_method=count($pay_methods);
	?>
	@if($numberPay_method==0)
		<div class="alert alert-warning">At present, There are not any Pay Method</div>
	@else
		<table class="table table-bordered">
			<tr>
				<th colspan="3">Payment Method</th>
			</tr>
			@foreach($pay_methods as $pay_method)
				<tr>
					<td>
						{{ $pay_method->name }}
					</td>
					<td>
						<a href="{{URL::asset("/pay_method") .'/'. $pay_method->id}}/edit" class="btn btn-primary btn-xs pull-left"><i class="fa fa-pencil"></i></a>

						{{Form::open(array('url' => 'pay_method'. '/' . $pay_method->id,'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure that you want to delete this payment method?")', 'class' => 'pull-left'))}}
							{{Form::button("<i class='fa fa-trash-o'></i>" , array('class' => 'btn btn-danger btn-xs', 'type' => 'submit'))}}
						{{Form::close()}}
					</td>
				</tr>
			@endforeach
		</table>
	@endif
@stop