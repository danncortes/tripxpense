@extends('layouts.base')

@section('titulo')
	Create a New Payment Method - 
@stop
@section('contenido')
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3">
			<div class="well">
				<h2 class="margin-t-0">Create a New Payment Method</h2>
				<hr>
				@if(Session::has('message'))
					<p class="alert alert-danger">{{Session::get('message')}}</p>
				@endif
				{{Form::open(array('route' => 'pay_method.store','method' => 'post'))}}
					<fieldset class="form-group">
						{{Form::label('name', 'Name: ')}}
						{{Form::text('name', Input::old('name'), array('class' => 'form-control', 'required'))}}
					</fieldset>
					@if($errors->has('name'))
						{{Form::label('name', $errors->first('name'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif
					{{Form::submit('Create', array('class' => 'btn btn-success btn-block'))}}
					{{Form::close()}}
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop