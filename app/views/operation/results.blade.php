@extends('layouts.base')

@section('titulo')
	Find results - 
@stop
@section('contenido')
	<h3>Operations Results</h3><br/>
	@if(Session::has('message'))
		<p class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{{Session::get('message')}}
		</p>
	@endif
	<div class"row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-6">
			<div class="searcher">
				@if(!isset($singleTravel))
					<form id="finderOperation" action="/operation/find" method="POST">
				@else
					<form id="finderOperation" action="/operationsTravel/find/{{$travel->id}}" method="POST">
				@endif
					<div class="input-group">
	          <input type="text" class="form-control" name="finder">
	          <span class="input-group-btn">
	            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
	          </span>
	        </div>
				</form>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<br/><br/>
	<?php
		$numberOperations=count($operations);
	?>
	@if($numberOperations==0)
		<div class="alert alert-warning">At present, you do not have any operations matching with the search</div>
	@else
		<div class="sorting-secc">

			<div class="btn-group sort-shape" data-toggle="buttons">
			  <label class="btn btn-default btn-sm active">
			    <input type="radio" name="sort-shape" id="item-block" checked>
			    <i class="fa fa-th"></i>
			  </label>
			  <label class="btn btn-default btn-sm">
			    <input type="radio" name="sort-shape" id="item-list">
			    <i class="fa fa-list"></i>
			  </label>
			</div>

			<div class="btn-group sort-direction" data-toggle="buttons">
			  <label class="btn btn-default btn-sm">
			    <input type="radio" name="sort-direction" data-sort-value="asc" id="item-updown">
			    <i class="fa fa-sort-amount-asc"></i>
			  </label>
			  <label class="btn btn-default btn-sm active">
			    <input type="radio" name="sort-direction" data-sort-value="desc"  id="item-downup" checked>
			    <i class="fa fa-sort-amount-desc"></i>
			  </label>
			</div>

			<span class="cap">Sort By</span>
			<div class="btn-group sort-type" data-toggle="buttons">
			  <label class="btn btn-default btn-sm active">
			    <input type="radio" data-sort-value="date" name="sort-type" checked>
			    Date
			  </label>
			  <label class="btn btn-default btn-sm">
			    <input type="radio" data-sort-value="title" name="sort-type">
					Title
			  </label>
			  <label class="btn btn-default btn-sm">
			    <input type="radio" data-sort-value="spent" name="sort-type">
			    Spent
			  </label>
			  @if(!isset($singleTravel))
				  <label class="btn btn-default btn-sm">
				    <input type="radio" data-sort-value="travel" name="sort-type">
				    Travel
				  </label>
			  @endif
			  <label class="btn btn-default btn-sm">
			    <input type="radio" data-sort-value="type" name="sort-type">
			    Type
			  </label>
			</div>

		</div>
		<br>
		<div class="operation-items isotope">
			@foreach($operations as $operation)
			<div class="elem-item">
				<div class="box">
					<p class="elem date">{{ $operation->date_op }}</p>
					<p class="elem title">{{ $operation->title }}</p>
					<?
						$codPay_method=$operation->cod_method;
						$method = Pay_method::where('id', $codPay_method)->get();
					?>
					@if($method[0]['name'] == 'CREDIT CARD')
						<i class="elem fa fa-credit-card warning type" data-type="creditcard"><span>Credit Card</span></i>
					@else
						<i class="elem fa fa-money success type" data-type="cash"><span>Cash</span></i>
					@endif
					<h2 class="elem spent">{{ $operation->cost }}</h2>
					<?
						$codTravel=$operation->cod_travel;
						$travel = Travel::where('id', $codTravel)->get();
					?>
					<p class="elem travel">{{	$travel[0]['name'];}}</p>
					<ul class="actions">
						<li>
							<a href="{{URL::asset("/operation") .'/'. $operation->id}}/edit" class="button circl-butt circl-butt-warning circl-butt-anima">
								<i class="fa fa-pencil"></i>
								<div class="back-circl"></div>
							</a>
						</li>
						<li>
							{{Form::open(array('url' => 'operation'. '/' . $operation->id,'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this operation?")', 'class' => 'pull-left'))}}
								<button class="button circl-butt circl-butt-danger circl-butt-anima" type="submit">
									<i class="fa fa-trash-o"></i>
									<div class="back-circl"></div>
								</button>
							{{Form::close()}}

						</li>
					</ul>
					
				</div>
			</div>
			@endforeach
		</div>
		<br>
	@endif
@stop

@section('libs')
	{{ HTML::script('/assets/js/libs/isotope.pkgd.min.js'); }}
	{{ HTML::script('/assets/js/operations/operation_list_view.js'); }}
	{{ HTML::script('/assets/js/list_view_scripts.js'); }}
@stop