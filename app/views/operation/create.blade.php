@extends('layouts.base')

@section('titulo')
	Create a New Operation - 
@stop
@section('contenido')
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
			<div class="well">
				<h2 class="margin-t-0">Create a New Operation</h2>
				<hr>

				

				@if(Session::has('message'))
					<p class="alert alert-danger">{{Session::get('message')}}</p>
				@endif
				{{Form::open(array('route' => 'operation.store','method' => 'post'))}}

					{{Form::hidden('title', Input::old('title'), array('class' => 'form-control', 'id' => 'title-ope'))}}
					<!-- @if($errors->has('title'))
						{{Form::label('title', $errors->first('title'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif -->
					<fieldset class="form-group">
						{{Form::label('cost', 'Spent: ')}}
						{{Form::text('cost', Input::old('cost'), array('class' => 'form-control', 'placeholder' => '$ How much did you spend?', 'required'))}}
					</fieldset>
					@if($errors->has('cost'))
						{{Form::label('cost', $errors->first('cost'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif
					
					

					<fieldset class="form-group">
						<div class="row">
							<div class="col-xs-12 col-sm-4">
								{{Form::label('cod_category', 'Commerce Category: ')}} 
							</div>
							<div class="col-xs-12 col-sm-8">
								<select id="cod_category" class="select" data-width="100%" name="cod_category">
									<option value="">-</option>
										<?php $selected = ''; ?>
								   @foreach($categories as $category)

										@if(Input::old('cod_category') == $category->id)
								  		{{$selected='selected'}}
								  	@else
								  		{{$selected=''}}
								  	@endif

										@if(isset($start_category))
											@if($start_category == $category->id)
									  		{{$selected='selected'}}
										  @else
										  	{{$selected=''}}
									  	@endif
										@endif

										{{'<option value="'.$category->id.'"'.$selected.'>'.$category->name.'</option>'}}
									@endforeach
								</select>	
							</div>
						</div>
					</fieldset>

					@if($errors->has('cod_category'))
						{{Form::label('cod_category', $errors->first('cod_category'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif


					<fieldset class="form-group">
						{{Form::label('cod_commerce', 'Commerce: ')}} 
						<div class="input-group">
							<input class="form-control" type="text" id="cod_commerce1" autocomplete="on" placeholder='Find a commerce or create a new one' required>	
							<input class="form-control" type="hidden" id="cod_commerce" name="cod_commerce">	
							<span class="input-group-btn">
								<div class="btn btn-default disabled create-commerce">Create</div>
							</span>
						</div>
						@if($errors->has('cod_commerce'))
							{{Form::label('cod_commerce', $errors->first('cod_commerce'), array('class' => 'alert alert-danger'))}}
							<br/>
						@endif
					</fieldset>

					<fieldset class="form-group">
						<div class="row">
							<div class="col-xs-12 col-sm-4">
								{{Form::label('cod_method', 'Pay Method: ')}} 
							</div>
							<div class="col-xs-12 col-sm-8">
								<select id="cod_method" class="select" data-width="100%" name="cod_method">
									<option value="">-</option>
								  @foreach($pay_methods as $pay_method)
								  	@if(Input::old('cod_method') == $pay_method->id)
								  		{{$selected='selected'}}
								  	@else
								  		{{$selected=''}}
								  	@endif
										
										<!-- this is from modal step1 -->
										@if(isset($start_pay_method))
											@if($start_pay_method == $pay_method->id)
									  		{{$selected='selected'}}
									  	@else
									  		{{$selected=''}}
									  	@endif
										@endif

										{{'<option value="'.$pay_method->id.'"'.$selected.'>'.$pay_method->name.'</option>'}}
									@endforeach
								</select>	
								@if($errors->has('cod_method'))
									{{Form::label('cod_method', $errors->first('cod_method'), array('class' => 'alert alert-danger'))}}
									<br/>
								@endif
							</div>
						</div>
					</fieldset>

					<fieldset class="form-group">
						<div class="row">
							<div class="col-xs-12 col-sm-4">
								{{Form::label('cod_travel', 'Travel: ')}} 
							</div>
							<div class="col-xs-12 col-sm-8">
								<select id="cod_travel" class="select" name="cod_travel" data-width="100%" data-live-search="true">
									<option value="">-</option>
								  @foreach($travels as $travel)
										
										@if(Input::old('cod_travel') == $travel->id)
								  		{{$selected='selected'}}
								  	@else
								  		{{$selected=''}}
								  	@endif

								  	@if(isset($start_travel))
											@if($start_travel == $travel->id)
									  		{{$selected='selected'}}
									  	@else
									  		{{$selected=''}}
									  	@endif
										@endif

										{{'<option value="'.$travel->id.'"'.$selected.'>'.$travel->name.'</option>'}}
									@endforeach
								</select>
								@if($errors->has('cod_travel'))
									{{Form::label('cod_travel', $errors->first('cod_travel'), array('class' => 'alert alert-danger'))}}
									<br/>
								@endif
							</div>
						</div>
					</fieldset>
					
					<div class="datepicker">
						<fieldset class="form-group">
							<div class="row">
								<div class="col-xs-12 col-sm-4">
									{{Form::label('date_op', 'Date: ')}}
								</div>
								<div class="col-xs-12 col-sm-8">
									{{Form::text('date_op', date('Y-m-d'), array('class' => 'form-control', 'required' ))}}
								</div>
							</div>
						</fieldset>
					</div>
					@if($errors->has('date_op'))
						{{Form::label('date_op', $errors->first('date_op'), array('class' => 'alert alert-danger', ))}}
						<br/>
					@endif
					<div class="optionals-fields-div">
						Optionals
					</div>
					<fieldset class="form-group">
						<div class="row">
							<div class="col-xs-12 col-sm-4">
								{{Form::label('neighborhood', 'Neighborhood: ')}} 
							</div>
							<div class="col-xs-12 col-sm-8">
								{{Form::text('neighborhood', Input::old('neighborhood'), array('class' => 'form-control neighborhood_field create', 'placeholder' => 'Neighborhood or direction where you bought'))}}
							</div>
						</div>
					</fieldset>
					@if($errors->has('neighborhood'))
						{{Form::label('neighborhood', $errors->first('neighborhood'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif

					<fieldset class="form-group">
						<div class="row">
							<div class="col-xs-12 col-sm-4">
								{{Form::label('cod_country', 'Country: ')}} 
							</div>
							<div class="col-xs-12 col-sm-8">
								<select id="cod_country" name="cod_country" class="select" data-width="100%" data-live-search="true">
									<option value="">-</option>
							    @foreach($country as $country)

										@if(Input::old('cod_country') == $country->code)
								  		{{$selected='selected'}}
								  	@else
								  		{{$selected=''}}
								  	@endif
								  	
										{{'<option value="'.$country->code.'">'.$country->Name.'</option>'}}
									@endforeach
								</select>	
							</div>
						</div>
					</fieldset>
					@if($errors->has('cod_country'))
						{{Form::label('cod_country', $errors->first('cod_country'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif

					<fieldset class="form-group" id="city-list-area">
						<div class="row">
							<div class="col-xs-12 col-sm-4">
								{{Form::label('cod_city', 'City: ')}} 
							</div>
							<div class="col-xs-12 col-sm-8 city-col">
								<select id="cod_city" name="cod_city" class="city-list select" data-width="100%" data-live-search="true">
									<option value="">Select a Country first</option>
								</select>	
								@if($errors->has('cod_city'))
									{{Form::label('cod_city', $errors->first('cod_city'), array('class' => 'alert alert-danger'))}}
									<br/>
								@endif
							</div>
						</div>
					</fieldset>


					<fieldset class="form-group">
						{{Form::label('description', 'Description: ')}}
						{{Form::text('description', Input::old('description'), array('class' => 'form-control', 'placeholder' => 'Optional'))}}
					</fieldset>
					@if($errors->has('description'))
						{{Form::label('description', $errors->first('description'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif

					{{Form::submit('Create', array('class' => 'btn btn-success btn-block'))}}
					{{Form::close()}}
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop
@section('libs')
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&language=en"></script>
	{{ HTML::script('/assets/js/operations/operation_create_edit_view.js'); }}
@stop