@extends('layouts.base')

@section('titulo')
	Edit Operation - 
@stop
@section('contenido')
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3">
			<div class="well">
				<h2 class="margin-t-0">Edit Operation</h2>
				<hr>



				@if(Session::has('message'))
					<p class="alert alert-danger">{{Session::get('message')}}</p>
				@endif
				{{Form::open(array('route' => array('operation.update', $operation->id),'method' => 'PUT'))}}
						{{Form::hidden('title', $operation->title, array('class' => 'form-control'))}}
<!-- 					<fieldset class="form-group">
						{{Form::label('title', 'Name: ')}}
					</fieldset>
					@if($errors->has('title'))
						{{Form::label('title', $errors->first('title'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif -->

					<fieldset class="form-group">
						{{Form::label('cost', 'Spent: ')}}
						{{Form::text('cost', $operation->cost, array('class' => 'form-control', 'placeholder' => '$ How much did you spend?', 'required'))}}
					</fieldset>
					@if($errors->has('cost'))
						{{Form::label('cost', $errors->first('cost'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif

					<fieldset class="form-group">
						<div class="row">
							<div class="col-xs-4">
								{{Form::label('cod_category', 'Commerce Category: ')}} 
							</div>
							<div class="col-xs-8">
								<select id="cod_category" class="select" data-width="100%" name="cod_category">
									<option value="">Select</option>
								    @foreach($categories as $category)
								    <?php
								  		if($commerce_cat == $category->id){
								  			$selected='selected';
								  		}
								  		else{
								  			$selected='';
								  		}
								  	?>
										{{'<option value="'.$category->id.'"'.$selected.'>'.$category->name.'</option>'}}
									@endforeach
								</select>	
							</div>
						</div>
					</fieldset>
					@if($errors->has('cod_category'))
						{{Form::label('cod_category', $errors->first('cod_category'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif
					
					

					
					<fieldset class="form-group">
						{{Form::label('cod_commerce', 'Commerce: ')}} 
						<div class="input-group">
							<input class="form-control" type="text" value="{{$operation->title}}" id="cod_commerce1" autocomplete="on" placeholder='Find a commerce or create a new one' required>	
							<input class="form-control" type="hidden" value="{{$operation->cod_commerce}}" id="cod_commerce" name="cod_commerce">	
							<span class="input-group-btn">
								<div class="btn btn-default disabled create-commerce">Create</div>
							</span>
						</div>
						@if($errors->has('cod_commerce'))
							{{Form::label('cod_commerce', $errors->first('cod_commerce'), array('class' => 'alert alert-danger'))}}
							<br/>
						@endif
					</fieldset>


					<fieldset class="form-group">
						<div class="row">
							<div class="col-xs-4">
								{{Form::label('cod_method', 'Pay Method: ')}} 
							</div>
							<div class="col-xs-8">
								<select id="cod_method" class="select" data-width="100%" name="cod_method">
									<option>Select</option>
								  @foreach($pay_methods as $pay_method)
								  	<?php
								  		if($operation->cod_method == $pay_method->id){
								  			$selected='selected';
								  		}
								  		else{
								  			$selected='';
								  		}
								  	?>
										{{'<option value="'.$pay_method->id.'"'.$selected.'>'.$pay_method->name.'</option>'}}
									@endforeach
								</select>	
							</div>
						</div>
					</fieldset>
					@if($errors->has('cod_method'))
						{{Form::label('cod_method', $errors->first('cod_method'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif

					<fieldset class="form-group">
						<div class="row">
							<div class="col-xs-4">
								{{Form::label('cod_travel', 'Travel: ')}} 
							</div>
							<div class="col-xs-8">
								<select id="cod_travel" class="select" name="cod_travel" data-width="100%" data-live-search="true">
									<option>Select</option>
								    @foreach($travels as $travel)
								    	@if($operation->cod_travel==$travel->id)
								    		{{$selected='selected'}}
								    	@else
								    		{{$selected=''}}
								    	@endif
										{{'<option value="'.$travel->id.'"'.$selected.'>'.$travel->name.'</option>'}}
									@endforeach
								</select>
								@if($errors->has('cod_travel'))
									{{Form::label('cod_travel', $errors->first('cod_travel'), array('class' => 'alert alert-danger'))}}
									<br/>
								@endif
							</div>
						</div>
					</fieldset>

					<div class="datepicker">
						<fieldset class="form-group">
							<div class="row">
								<div class="col-xs-12 col-sm-4">
									{{Form::label('date_op', 'Date: ')}}
								</div>
								<div class="col-xs-12 col-sm-8">
									{{Form::text('date_op', $operation->date_op, array('class' => 'form-control', 'required'))}}
								</div>
							</div>
						</fieldset>
					</div>
					@if($errors->has('date_op'))
						{{Form::label('date_op', $errors->first('date_op'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif
					<div class="optionals-fields-div">
						Optionals
					</div>
					<fieldset class="form-group">
						<div class="row">
							<div class="col-xs-4">
								{{Form::label('neighborhood', 'Neighborhood: ')}}
							</div>
							<div class="col-xs-8">
								{{Form::text('neighborhood', $operation->neighborhood, array('class' => 'form-control neighborhood_field ', 'placeholder' => 'Neighborhood or direction where you bought'))}}
							</div>
						</div>
					</fieldset>
					@if($errors->has('neighborhood'))
						{{Form::label('neighborhood', $errors->first('neighborhood'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif

					<!-- por aqui voy -->
					<fieldset class="form-group">
						<div class="row">
							<div class="col-xs-4">
								{{Form::label('cod_country', 'Country: ')}} 
							</div>
							<div class="col-xs-8">
								<select id="cod_country" name="cod_country" class="select" data-width="100%" data-live-search="true">
									<option value="">Select</option>
								    @foreach($country as $country)

								    <?php
								  		if($cod_country == $country->code){
								  			$selected='selected';
								  		}
								  		else{
								  			$selected='';
								  		}
								  	?>

										{{'<option value="'.$country->code.'"'.$selected.'>'.$country->Name.'</option>'}}
									@endforeach
								</select>	
							</div>
						</div>
					</fieldset>
					@if($errors->has('cod_country'))
						{{Form::label('cod_country', $errors->first('cod_country'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif

					<fieldset class="form-group" id="city-list-area">
						<div class="row">
							<div class="col-xs-4">
								{{Form::label('cod_city', 'City: ')}} 
							</div>
							<div class="col-xs-8 city-col">
								<select id="cod_city" name="cod_city" class="city-list select" data-width="100%" data-live-search="true">
									<option value="">Select</option>
									@foreach($cities as $cities)
								    <?php
								  		if($operation->cod_city == $cities->ID){
								  			$selected='selected';
								  		}
								  		else{
								  			$selected='';
								  		}
								  	?>
										{{'<option value="'.$cities->ID.'"'.$selected.'>'.$cities->Name.'</option>'}}
									@endforeach
								</select>	
							</div>
						</div>
					</fieldset>



					<fieldset class="form-group">
						{{Form::label('description', 'Description: ')}}
						{{Form::text('description', $operation->description, array('class' => 'form-control', 'placeholder' => 'Optional'))}}
					</fieldset>
					@if($errors->has('description'))
						{{Form::label('description', $errors->first('description'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif


					{{Form::submit('Update', array('class' => 'btn btn-success btn-block'))}}
					{{Form::close()}}
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop
@section('libs')
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&language=en"></script>
	{{ HTML::script('/assets/js/operations/operation_create_edit_view.js'); }}
@stop