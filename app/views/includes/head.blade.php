<head>  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="Take control of your budget and expenses while traveling">
  <title>
    @section('titulo')
    @show
    Tripxpense : Take control of your expenses while traveling
  </title>
  <!-- {{ HTML::style('/assets/bootstrap/bootstrap.less'); }} -->
  <!-- {{ HTML::style('/assets/less/slate-bootstrap.min.css'); }} -->
  {{ HTML::style('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'); }}
  <link rel="stylesheet" href="{{ URL::asset('/assets/datepicker/css/datepicker.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('/assets/bootstrap-select/css/bootstrap-select.min.css') }}">
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
  <link rel="stylesheet/less" href="{{ URL::asset('/assets/less/styles.less') }}">
  {{ HTML::script('/assets/js/libs/less-1.5.0.min.js'); }}
  @include('includes.analytics')
</head>