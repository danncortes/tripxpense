<footer>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-7">
				<div class="logo-footer">
					<img src="/assets/img/logo-tripxpense.png" alt="">
				</div>
				<div class="legal">
					<p>© All rights reserved</p>
				</div>
			</div>
			<div class="col-xs-5 txt-right">
				<ul class='social-autor'>
					<!-- <li>
						<a class="autor" data-toggle="popover" data-placement="top" title="Daniel Cortés" data-content="">
							@danncortes
						</a>
					</li> -->
					<li>
						<a href='http://ve.linkedin.com/in/danncortes/en' target='blank'><i class='fa fa-linkedin-square fa-2x'></i></a>
					</li>
					<li>
						<a href='https://bitbucket.org/danncortes' target='blank'><i class='fa  fa-bitbucket-square fa-2x'></i></a>
					</li>
					<li>
						<a href='http://twitter.com/danncortes' target='blank'><i class='fa  fa-twitter-square fa-2x'></i></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</footer>