@extends('layouts.welcome_base')

@section('titulo')
@stop
@section('contenido')
	<header  class="header-welcome">
		<section class="container-fluid normal-h">
			<div class="logo">
				<img src="assets/img/logo-tripxpense.png" alt="">
			</div>
			<nav class="nav-header">
				<ul>
					<li class="login"><a href="/login" title="Login"><span>Login</span> <i class="fa fa-sign-in fa-1x"></i> <i class="fa fa-sign-in fa-2x"></i></a> </li>
				</ul>
			</nav>
		</section>
	</header>
	<main class="welcome">
		<div class="cont-slider">
			<div class="textslider">
				<h1 class="title">Take control of your expenses while traveling</h1>
				<div>
					<a href="/user/create" class="btn btn-lg btn-success col-xs-offset-2 col-xs-8 col-sm-offset-4 col-sm-4 signup">
						SIGN UP NOW
					</a>
				</div>
			</div>
			<section class="slider">
	      <div class="flexslider">
	        <ul class="slides">
	          <li style="background-image:url('assets/img/spendtrip-slide1.jpg');"></li>
	          <li style="background-image:url('assets/img/spendtrip-slide2.jpg');"></li>
	          <li style="background-image:url('assets/img/spendtrip-slide3.jpg');"></li>
	          <li style="background-image:url('assets/img/spendtrip-slide4.jpg');"></li>
	          <li style="background-image:url('assets/img/spendtrip-slide5.jpg');"></li>
	          <li style="background-image:url('assets/img/spendtrip-slide6.jpg');"></li>
	        </ul>
	      </div>
	    </section>
		</div>
		<section class="claims">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="item">
							<div class="icon">
								<img src="assets/img/icons/icon-map.png" alt="Create travels">
							</div>
							<h2 class="title">Create travels</h2>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="item">
							<div class="icon">
								<img src="assets/img/icons/icon-wallet.png" alt="Create travels">
							</div>
							<h2 class="title">Register spends</h2>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="item">
							<div class="icon">
								<img src="assets/img/icons/icon-calc.png" alt="Create travels">
							</div>
							<h2 class="title">Take the control of budget</h2>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="screen-dash">
			<div class="container-fluid">
				<div class="print">
					<img src="assets/img/dashboard.png" alt="">
				</div>
				<br>
				<br>
			</div>
		</section>
		<section>
			<div class="container-fluid txt-center">
				<div class="row">
					<a href="/user/create" class="btn btn-lg btn-success col-xs-offset-2 col-xs-8 col-sm-offset-4 col-sm-4 ">
						SIGN UP NOW
					</a>
				</div>
				<br><br>
				<br>
			</div>
		</section>
	</main>
	@include('includes.footer')
@stop