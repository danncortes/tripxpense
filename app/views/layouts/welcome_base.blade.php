<!DOCTYPE html>
<html lang="en">
@include('includes.head')
<body>
	
@yield('contenido')

<!-- page content -->
{{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'); }}
{{ HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'); }}
{{ HTML::script('/assets/datepicker/js/bootstrap-datepicker.js'); }}
{{ HTML::script('/assets/bootstrap-select/js/bootstrap-select.min.js'); }}
{{ HTML::style('/assets/flexslider/flexslider.css'); }}
{{ HTML::script('/assets/flexslider/jquery.flexslider-min.js'); }}

<script type="text/javascript">
  
$(function(){

  /** FLEXSLIDER **/
  var contFlexslider = $('.cont-slider')
  var docum = $(document)
  var windo = $(window)
  var header = $('header')

  setHeightSlider()

  $(window).load(function() {
    $('.flexslider').flexslider({
      animation: "fade",
      controlNav: false,
      directionNav: false
    });
  });

  function setHeightSlider(){
    contFlexslider.height(windo.height() - header.height())
  }
  /** FLEXSLIDER **/
  windo.resize(function(){
    setHeightSlider()
  })
})

</script>
{{ HTML::script('/assets/js/scripts.js'); }}


</body>
</html>