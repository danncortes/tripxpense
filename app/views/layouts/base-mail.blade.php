<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
    <table bgcolor="#eee" width="100%" cellpadding="50" cellspacing="0">
      <tr>
        <td align="center">
          <table width="600px" bgcolor="white" cellpadding="0" cellspacing="0">
            <tr>
              <td>
                <table bgcolor="#151719" cellpadding="26" cellspacing="0" width="100%" height="60px">
                  <tr>
                    <td valign="middle">
                      <img src="http://tripxpense.com/assets/img/logo-tripxpense.png" alt="tripxpense.com" title="tripxpense.com" width="170px;" height="auto">
                    </td>
                  </tr>
                </table>
                <table cellpadding="26" cellspacing="0" width="100%">
                  <tr>
                    <td>
											@yield('contenido')
										</td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
	</body>
</html>
