<!DOCTYPE html>
<html lang="en">
@include('includes.head')
<body>
	<header>
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
			  <!-- Brand and toggle get grouped for better mobile display -->
			  <div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			      <span class="sr-only">Toggle navigation</span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			    </button>
			    @if(Auth::check())
			    	<a class="navbar-brand logo-in" href='{{URL::asset("/dashboard")}}'>
			    @else
			    	<a class="navbar-brand logo-in" href='{{URL::asset("/")}}'>
			    @endif
			    	<img src="/assets/img/logo-tripxpense.png" alt="">
			    </a>
			  </div>

			  <!-- Collect the nav links, forms, and other content for toggling -->
			  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    <ul class="nav navbar-nav">
			    @if(!Auth::check())
			    	<li class=""><a href="{{URL::asset('/login')}}">Login</a></li>
			      	<li class=""><a href="{{URL::asset('/user/create')}}">Sign Up</a></li>
			   	@else
			   		@if(Auth::user()->level>0)
			      		<li><a href="{{URL::asset('/user')}}">Users</a></li>
			      		<li class=""><a href="{{URL::asset('/pay_method')}}">Payment Methods</a></li>
			      		<li class=""><a href="{{URL::asset('/category')}}">Commerce Categories</a></li>
			      	@endif
			      	<li class=""><a href="{{URL::asset('/operation')}}">Operations</a></li>
			      	<li class=""><a href="{{URL::asset('/commerce')}}">Commerces</a></li>
			      	<li class=""><a href="{{URL::asset('/travel')}}">Travels</a></li>
			    </ul>
			    <ul class="nav navbar-nav navbar-right">
			    	<li><a href="" class="dropdown-toggle" data-toggle="dropdown">Profile <b class="caret"></b></a>
				    	<ul class="dropdown-menu">
							<li><a href='{{URL::asset("/dashboard")}}'>Dashboard</a></li>
							<li><a href='{{URL::asset("/profile")}}'>Profile</a></li>
							<li><a href="{{URL::asset('/logout')}}">Logout</a></li>
						</ul>
					</li>
			    @endif
			      
			    </ul>
			  </div><!-- /.navbar-collapse -->
			</div>
		</nav>
	</header>
	<main class="int">
		<section class="container-fluid">
			
			@yield('contenido')
		</section>
	</main>
	@if(Auth::check())
		@if(count($travels)<=0)
			<div class="newTravelButton">
				<i class="fa fa-plus"></i>
			</div>
		@else
			<div class="newOperationButton">
				<i class="fa fa-plus"></i>
			</div>
		@endif
	@endif
	@include('includes.footer')

	@if(Auth::check())
		<!-- Modal New Travel-->
		<div class="modal fade" id="startingTravel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel2">New Travel</h4>
		      </div>
		      	{{Form::open(array('route' => 'travel.store','method' => 'post'))}}
				      <div class="modal-body">
				      	
				      	@if(Session::has('message'))
									<p class="alert alert-danger">{{Session::get('message')}}</p>
								@endif
								{{Form::open(array('route' => 'travel.store','method' => 'post'))}}
									<fieldset class="form-group">
										{{Form::label('name', 'Name: ')}}
										{{Form::text('name', Input::old('name'), array('class' => 'form-control', 'required'))}}
									</fieldset>
									@if($errors->has('name'))
										{{Form::label('name', $errors->first('name'), array('class' => 'alert alert-danger'))}}
										<br/>
									@endif
									
									<div class="row">
										<div class="col-xs-12 col-md-6">

											<div class="datepicker">
												<fieldset class="form-group">
													{{Form::label('start', 'Start Date: ')}}
													{{Form::text('start', Input::old('start'), array('class' => 'form-control', 'required'))}}
												</fieldset>
											</div>
											@if($errors->has('start'))
												{{Form::label('start', $errors->first('start'), array('class' => 'alert alert-danger'))}}
												<br/>
											@endif

										</div>
										<div class="col-xs-12 col-md-6">
											
											<div class="datepicker">
												<fieldset class="form-group">
													{{Form::label('finish', 'Finish Date: ')}}
													{{Form::text('finish', Input::old('finish'), array('class' => 'form-control', 'required'))}}
												</fieldset>
											</div>
											@if($errors->has('finish'))
												{{Form::label('finish', $errors->first('finish'), array('class' => 'alert alert-danger'))}}
												<br/>
											@endif

										</div>
									</div>

									

									<fieldset class="form-group">
										{{Form::label('cash_balance', 'Cash Balance: ')}}
										{{Form::text('cash_balance', Input::old('cash_balance'), array('class' => 'form-control', 'required'))}}
									</fieldset>
									@if($errors->has('cash_balance'))
										{{Form::label('cash_balance', $errors->first('cash_balance'), array('class' => 'alert alert-danger'))}}
										<br/>
									@endif

									<fieldset class="form-group">
										{{Form::label('tdc_balance', 'Credit Card Balance: ')}}
										{{Form::text('tdc_balance', Input::old('tdc_balance'), array('class' => 'form-control', 'required'))}}
									</fieldset>
									@if($errors->has('tdc_balance'))
										{{Form::label('tdc_balance', $errors->first('tdc_balance'), array('class' => 'alert alert-danger'))}}
										<br/>
									@endif

									<fieldset class="form-group">
										{{Form::label('tdd_balance', 'Debit Card Balance: ')}}
										{{Form::text('tdd_balance', Input::old('tdd_balance'), array('class' => 'form-control', 'required'))}}
									</fieldset>
									@if($errors->has('tdd_balance'))
										{{Form::label('tdd_balance', $errors->first('tdd_balance'), array('class' => 'alert alert-danger'))}}
										<br/>
									@endif

									{{Form::submit('Create', array('class' => 'btn btn-success btn-block'))}}

				      </div>
		      	{{Form::close()}}
		    </div>
		  </div>
		</div>

		<!-- Modal New Operation-->
		<div class="modal fade" id="startingOperation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">New Operation</h4>
		      </div>
		      	{{Form::open(array('route' => 'operation.create','method' => 'post'))}}
				      <div class="modal-body">
				      	
				      	<div class="content">
					      	
				      	</div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				        <button type="submit" class="btn btn-success">Next <i class="fa fa-angle-right"></i></button>
				      </div>
		      	{{Form::close()}}
		    </div>
		  </div>
		</div>
	@endif


<!-- page content -->
{{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'); }}
{{ HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js'); }}
{{ HTML::script('/assets/datepicker/js/bootstrap-datepicker.js'); }}
{{ HTML::script('/assets/bootstrap-select/js/bootstrap-select.min.js'); }}
{{ HTML::script('/assets/js/libs/jquery-ui.min.js'); }}
{{ HTML::script('/assets/js/scripts.js'); }}
	@yield('libs')
</body>
</html>