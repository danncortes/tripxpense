@extends('layouts.base')

@section('titulo')
	Create a New Commerce Category - 
@stop
@section('contenido')
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3">
			<div class="well">
				<h2 class="margin-t-0">Create a New Commerce Category</h2>
				<hr>
				@if(Session::has('message'))
					<p class="alert alert-danger">{{Session::get('message')}}</p>
				@endif
				{{Form::open(array('route' => 'category.store','method' => 'post'))}}
					<fieldset class="form-group">
						{{Form::label('name', 'Name: ')}}
						{{Form::text('name', Input::old('name'), array('class' => 'form-control'))}}
					</fieldset>
					@if($errors->has('name'))
						{{Form::label('name', $errors->first('name'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif
					{{Form::submit('Create', array('class' => 'btn btn-primary'))}}
					{{Form::close()}}
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop