@extends('layouts.base')

@section('titulo')
	Password Remind - 
@stop
@section('contenido')
	<div class="row txtcenter">
		<div class="col-xs-6 col-xs-offset-3">
			<div class="well">
				<h2 class="margin-t-0">Password reminder</h2>
				<hr/>
				@if(Session::has('error'))
					<p class="alert alert-danger">{{Session::get('error')}}</p>
				@elseif(Session::has('success'))
					<p class="alert alert-success">{{Session::get('success')}}</p>
				@endif
				<form action="{{ action('RemindersController@postRemind') }}" method="POST">
					<div class="form-group">
			    	<input class='form-control' type="email" name="email">	
					</div>
			  	<input type="submit" class='btn btn-default' value="Send Reminder">
				</form>
			</div>
		</div>
	</div>
@stop