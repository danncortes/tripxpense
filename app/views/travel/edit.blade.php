@extends('layouts.base')

@section('titulo')
	Edit Travel - 
@stop
@section('contenido')
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			<div class="well">
				<h2 class="margin-t-0">Edit Travel</h2>
				<hr>
				@if(Session::has('message'))
					<p class="alert alert-danger">{{Session::get('message')}}</p>
				@endif
				{{Form::open(array('route' => array('travel.update', $travel->id), 'method' => 'PUT'))}}
					<fieldset class="form-group">
						{{Form::label('name', 'Name: ')}}
						{{Form::text('name', $travel->name, array('class' => 'form-control', 'required'))}}
					</fieldset>
					@if($errors->has('name'))
						{{Form::label('name', $errors->first('name'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif

					<div class="row">
						<div class="col-xs-12 col-md-6">

							<div class="datepicker">
								<fieldset class="form-group">
									{{Form::label('start', 'Start Date: ')}}
									{{Form::text('start', $travel->start, array('class' => 'form-control', 'required'))}}
								</fieldset>
							</div>
							@if($errors->has('start'))
								{{Form::label('start', $errors->first('start'), array('class' => 'alert alert-danger'))}}
								<br/>
							@endif

						</div>
						<div class="col-xs-12 col-md-6">

							<div class="datepicker">
							<fieldset class="form-group">
								{{Form::label('finish', 'Finish Date: ')}}
								{{Form::text('finish', $travel->finish, array('class' => 'form-control', 'required'))}}
							</fieldset>
							</div>
							@if($errors->has('finish'))
								{{Form::label('finish', $errors->first('finish'), array('class' => 'alert alert-danger'))}}
								<br/>
							@endif

						</div>
					</div>


					<div class="row">
						<div class="col-sm-4">
							<fieldset class="form-group">
								{{Form::label('cash_balance', 'Cash Balance: ')}}
								{{Form::text('cash_balance', $travel->cash_balance, array('class' => 'form-control', 'required'))}}
							</fieldset>
							@if($errors->has('cash_balance'))
								{{Form::label('cash_balance', $errors->first('cash_balance'), array('class' => 'alert alert-danger'))}}
								<br/>
							@endif
							
						</div>
						<div class="col-sm-4">
							<fieldset class="form-group">
								{{Form::label('tdc_balance', 'Credit Card Balance: ')}}
								{{Form::text('tdc_balance', $travel->tdc_balance, array('class' => 'form-control', 'required'))}}
							</fieldset>
							@if($errors->has('tdc_balance'))
								{{Form::label('tdc_balance', $errors->first('tdc_balance'), array('class' => 'alert alert-danger'))}}
								<br/>
							@endif
							
						</div>
						<div class="col-sm-4">
							<fieldset class="form-group">
								{{Form::label('tdd_balance', 'Debit Card Balance: ')}}
								{{Form::text('tdd_balance', $travel->tdd_balance, array('class' => 'form-control', 'required'))}}
							</fieldset>
							@if($errors->has('tdd_balance'))
								{{Form::label('tdd_balance', $errors->first('tdd_balance'), array('class' => 'alert alert-danger'))}}
								<br/>
							@endif
							
						</div>
					</div>



					{{Form::submit('Save', array('class' => 'btn btn-success btn-block'))}}
					{{Form::close()}}
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop