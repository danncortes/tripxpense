@extends('layouts.base')

@section('titulo')
	Travels - 
@stop
@section('contenido')
	<h3>Travels</h3><br/>
	@if(Session::has('message'))
		<p class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{{Session::get('message')}}
		</p>
	@endif
	<a href="{{URL::asset('/travel/create')}}" class="btn btn-success col-xs-12 col-sm-4"><i class="fa fa-plus"></i> TRAVEL</a><br/><br/>
	<br>
	<?php
		$numberTravel=count($travels);
	?>
	@if($numberTravel==0)
		<div class="alert alert-warning">At present, you do not have any travel</div>
	@else
		
		<div class="sorting-secc">

			<div class="btn-group sort-shape" data-toggle="buttons">
			  <label class="btn btn-default btn-sm active">
			    <input type="radio" name="sort-shape" id="item-block" checked>
			    <i class="fa fa-th"></i>
			  </label>
			  <label class="btn btn-default btn-sm">
			    <input type="radio" name="sort-shape" id="item-list">
			    <i class="fa fa-list"></i>
			  </label>
			</div>

			<div class="btn-group sort-direction" data-toggle="buttons">
			  <label class="btn btn-default btn-sm">
			    <input type="radio" name="sort-direction" data-sort-value="asc" id="item-updown">
			    <i class="fa fa-sort-amount-asc"></i>
			  </label>
			  <label class="btn btn-default btn-sm active">
			    <input type="radio" name="sort-direction" data-sort-value="desc"  id="item-downup" checked>
			    <i class="fa fa-sort-amount-desc"></i>
			  </label>
			</div>

			<span class="cap">Sort By</span>
			<div class="btn-group sort-type" data-toggle="buttons">
			  <label class="btn btn-default btn-sm active">
			    <input type="radio" data-sort-value="name" name="sort-type" checked>
			    Name
			  </label>
			  <label class="btn btn-default btn-sm">
			    <input type="radio" data-sort-value="start" name="sort-type">
					Start
			  </label>
			  <label class="btn btn-default btn-sm">
			    <input type="radio" data-sort-value="finish" name="sort-type">
			    Finish
			  </label>
			  <label class="btn btn-default btn-sm">
			    <input type="radio" data-sort-value="cash" name="sort-type">
			    Cash
			  </label>
			  <label class="btn btn-default btn-sm">
			    <input type="radio" data-sort-value="creditcard" name="sort-type">
			    Credit Card
			  </label>
			  <label class="btn btn-default btn-sm">
			    <input type="radio" data-sort-value="debitcard" name="sort-type">
			    Debit Card
			  </label>
			</div>

		</div>

		<br>

		<div class="travel-items isotope">
			@foreach($travels as $travel)
			<div class="elem-item">
				<div class="box">
					<a href="{{URL::asset("/travel") .'/'. $travel->id}}">

						<p class="elem name">{{ $travel->name }}</p>

						<div class="date datestart">
							<span class="cap">Start</span>
							<p class="dat start">{{ $travel->start }}</p>
						</div>
						<div class="date datefinish">
							<span class="cap">Finish</span>
							<p class="dat finish">{{ $travel->finish }}</p>
						</div>

						<div class="budget">

							<div class="item card" title="Credit Card">
								<i class="fa fa-credit-card"></i>
								<p class="dat creditcard">$ {{ $travel->tdc_balance }}</p>
							</div>
							<div class="item debit" title="Debit Card">
								<i class="fa fa-credit-card"></i>
								<p class="dat debitcard">$ {{ $travel->tdd_balance }}</p>
							</div>
							<div class="item cash" title="Cash">
								<i class="fa fa-money"></i>
								<p class="dat cash">$ {{ $travel->cash_balance }}</p>
							</div>

						</div>

						
					</a>
					<ul class="actions">
						<li>
							<a href="{{URL::asset("/travel") .'/'. $travel->id}}/edit" class="button circl-butt circl-butt-warning circl-butt-anima">
								<i class="fa fa-pencil"></i>
								<div class="back-circl"></div>
							</a>
						</li>
						<li>
							{{Form::open(array('url' => 'travel'. '/' . $travel->id,'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this travel?")', 'class' => 'pull-left'))}}
								<button class="button circl-butt circl-butt-danger circl-butt-anima" type="submit">
									<i class="fa fa-trash-o"></i>
									<div class="back-circl"></div>
								</button>
							{{Form::close()}}

						</li>
					</ul>
					
				</div>
			</div>
			@endforeach
		</div>



	@endif
@stop

@section('libs')
	{{ HTML::script('/assets/js/libs/isotope.pkgd.min.js'); }}
	{{ HTML::script('/assets/js/travels/travel_list_view.js'); }}
	{{ HTML::script('/assets/js/list_view_scripts.js'); }}
@stop