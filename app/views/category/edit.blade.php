@extends('layouts.base')

@section('titulo')
	Edit Commerce Category - 
@stop
@section('contenido')
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3">
			<div class="well">
				<h2 class="margin-t-0">Edit Commerce Category</h2>
				<hr>
				@if(Session::has('message'))
					<p class="alert alert-danger">{{Session::get('message')}}</p>
				@endif
				{{Form::open(array('route' => array('category.update', $category->id), 'method' => 'PUT', 'files'=> true))}}
					<fieldset class="form-group">
						{{Form::label('name', 'Name: ')}}
						{{Form::text('name', $category->name, array('class' => 'form-control', 'required'))}}
					</fieldset>
					@if($errors->has('name'))
						{{Form::label('name', $errors->first('name'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif
					<fieldset class="form-group form-picture-category">
						{{Form::label('name', 'Icon ')}}
						<div class="edit-pic-category-area">
							@if(empty($category->pic))
								{{Form::file('icon', array('class' => 'form-control pointer'), Input::old('icon'))}}
								<div class="edit-pic-category hidden">
							@else
								{{Form::file('icon', array('class' => 'form-control pointer hidden'), Input::old('icon'))}}
								<div class="edit-pic-category">
							@endif
									<figure style="background-image:url('/{{$category->pic}}');"></figure>
									<div class="actions">
										<a class="button circl-butt circl-butt-danger circl-butt-anima" data-id="{{$category->id}}">
											<i class="fa fa-trash"></i>
											<div class="back-circl"></div>
										</a>
									</div>
								</div>
						</div>
						
					</fieldset>
					@if($errors->has('pic'))
						{{Form::label('pic', $errors->first('pic'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif

					<fieldset class="form-group">
						<div class="row">
							<div class="col-xs-12 col-sm-3">
									{{Form::label('sort', 'Position Sort: ')}}
							</div>
							<div class="col-xs-12 col-sm-9">
								<select class="select" data-width="100%" name="sort">
										<option value="">Select position</option>
										<?php 
											$numberCategory=count($categories);
											$selected="";
										 ?>
										 @for ($i = 1; $i <= $numberCategory; $i++)
										 	@if($category->sort==$i)
												<?php $selected="selected"; ?>
										 	@else
												<?php $selected=""; ?>
										 	@endif
											<option value="{{$i}}" {{$selected}}>{{$i}}</option>
										 @endfor
									</select>	
							</div>
						</div>
					</fieldset>
					@if($errors->has('sort'))
						{{Form::label('sort', $errors->first('sort'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif
					{{Form::submit('Save', array('class' => 'btn btn-success btn-block'))}}
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop

@section('libs')
	{{ HTML::script('/assets/js/categories/category_edit_view.js'); }}
@stop