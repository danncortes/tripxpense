@extends('layouts.base')

@section('titulo')
	Commerce Categories - 
@stop
@section('contenido')
	<h3>Commerce Categories</h3><br/>
	@if(Session::has('message'))
		<p class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{{Session::get('message')}}
		</p>
	@endif
	<a href="{{URL::asset('/category/create')}}" class="btn btn-success"><i class="fa fa-plus"></i> Category</a><br/><br/>
	<?php
		$numberCategory=count($categories);
	?>
	@if($numberCategory==0)
		<div class="alert alert-warning">At present, There are not any category</div>
	@else
		<table class="table table-bordered">
			<tr>
				<th>Name</th>
				<th>Icon</th>
				<th>Sort</th>
				<th></th>
			</tr>
			@foreach($categories as $category)
				<tr>
					<td>
						{{ $category->name }}
					</td>
					<td>
						@if($category->pic == '')
							No icon set yet
						@else
							<img src="{{ $category->pic }}" width="40" height="40" alt="">
						@endif
					</td>
					<td>
						{{ $category->sort . ' / ' . $numberCategory }}
					</td>
					<td>
		<!-- 				{{Form::open(array('url' => 'category'. '/' . $category->id,'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this category?")', 'class' => 'pull-left'))}}
							{{Form::button("<i class='fa fa-pencil'></i>" , array('class' => 'btn btn-primary btn-xs', 'type' => 'submit'))}}
						{{Form::close()}} -->
						<a href="{{URL::asset("/category") .'/'. $category->id}}/edit" class="btn btn-primary btn-xs pull-left"><i class="fa fa-pencil"></i></a>

						{{Form::open(array('url' => 'category'. '/' . $category->id,'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this category?")', 'class' => 'pull-left'))}}
							{{Form::button("<i class='fa fa-trash-o'></i>" , array('class' => 'btn btn-danger btn-xs', 'type' => 'submit'))}}
						{{Form::close()}}
					</td>
				</tr>
			@endforeach
		</table>
		<div class="txt-center">{{ $categories->links() }}</div>
	@endif
@stop