@extends('layouts.base')

@section('titulo')
	Create a New Commerce Category - 
@stop
@section('contenido')
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3">
			<div class="well">
				<h2 class="margin-t-0">Create a New Commerce Category</h2>
				<hr>
				@if(Session::has('message'))
					<p class="alert alert-danger">{{Session::get('message')}}</p>
				@endif
				{{Form::open(array('route' => 'category.store','method' => 'post', 'files'=> true))}}
					<fieldset class="form-group">
						{{Form::label('name', 'Name: ')}}
						{{Form::text('name', Input::old('name'), array('class' => 'form-control', 'required'))}}
					</fieldset>
					@if($errors->has('name'))
						{{Form::label('name', $errors->first('name'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif
					
					<fieldset class="form-group">
						{{Form::label('icon', 'Icon: ')}}
						{{Form::file('icon', array('class' => 'form-control'), Input::old('icon'))}}

					</fieldset>
					<fieldset class="form-group">
						<div class="row">
							<div class="col-xs-12 col-sm-3">
									{{Form::label('sort', 'Position Sort: ')}}
							</div>
							<div class="col-xs-12 col-sm-9">
								<select class="select" data-width="100%" name="sort">
										<option value="">Select position</option>
										<?php 
											$numberCategory=count($categories);
										 ?>
										 @for ($i = 1; $i <= $numberCategory; $i++)
												<option value="{{$i}}">{{$i}}</option>
										 @endfor
									</select>	
							</div>
						</div>
					</fieldset>
					@if($errors->has('sort'))
					{{Form::label('sort', $errors->first('sort'), array('class' => 'alert alert-danger'))}}
						<br/>
					@endif
					{{Form::submit('Create', array('class' => 'btn btn-success btn-block'))}}
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop