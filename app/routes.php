<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array('before' => 'desauth'), function(){
	Route::get('/', function(){ return View::make('welcome'); });
});

/*USER*/
/*Login*/
// Route::get('/login', 'UserController@login');
Route::get('/login', array('uses' => 'UserController@login', 'before' => 'desauth'));
Route::post('/authenticate', 'UserController@authenticate');
Route::get('/logout', array('uses' => 'UserController@logout', 'before' => 'auth'));

Route::resource('user', 'UserController');
Route::post('/user/updatelevel/{id}', array('as' => 'user.updatelevel', 'uses' => 'UserController@updatelevel'));
Route::get('/profile', array('uses' => 'UserController@showAuthUser', 'before' => 'auth'));
Route::get('/dashboard', array('uses'=>'UserController@dashboard', 'before' => 'auth'));

Route::group(array('before' => 'desauth'), function(){
	Route::get('/password/remind', array('uses' => 'RemindersController@getRemind'));
	Route::post('/password/remind', array( 'uses' => 'RemindersController@postRemind'));
	
	Route::get('/password/reset/{token}', array( 'uses' => 'RemindersController@getReset'));

	Route::post('/password/reset/{token}', array( 'uses' => 'RemindersController@postReset'));
});

/*PAY_METHODS*/
Route::group(array('before' => 'auth'), function(){
	Route::resource('pay_method', 'Pay_methodController');
});

/*TRAVELS*/
Route::group(array('before' => 'auth'), function(){ /*With the group, We can group with a function, the routes which would be executed if exist a logged user*/
	Route::resource('travel', 'TravelController');
});
/*CATEGORY COMMERCES*/
Route::group(array('before' => 'auth'), function(){ /*With the group, We can group with a function, the routes which would be executed if exist a logged user*/
	Route::resource('category', 'CategoryController');
	Route::post('/removePic/{id}', array('uses' => 'CategoryController@removePic'));
});
/*COMMERCES*/
Route::group(array('before' => 'auth'), function(){ /*With the group, We can group with a function, the routes which would be executed if exist a logged user*/
	Route::resource('commerce', 'CommerceController');
	Route::post('/commerce/quickCreate/{datos}', array('uses' => 'CommerceController@quickCreate'));
});
/*OPERATIONS*/
Route::group(array('before' => 'auth'), function(){ /*With the group, We can group with a function, the routes which would be executed if exist a logged user*/
	Route::resource('operation', 'OperationController');
	Route::post('/operation/create', array('uses' => 'OperationController@create'));
	Route::get('/operationsTravel/{id}', array('uses' => 'OperationController@operationsTravel'));
	Route::post('/findcommerce/{id}', array('uses' => 'CommerceController@findCommerces'));
	Route::post('/getcities/{id}', array('uses' => 'CityController@getcities'));
	Route::post('/operation/find', array('uses' => 'OperationController@findOperations'));
	Route::post('/operationsTravel/find/{id}', array('uses' => 'OperationController@findOperationsTravel'));
	Route::post('/startOperation', array('uses' => 'OperationController@startingOperation'));
	Route::post('/statsOperations', array('uses' => 'OperationController@statsOperations2'));
	Route::post('/statsOperations/{id}', array('uses' => 'OperationController@statsOperations_travel'));
});
// Route::get('/test', array('uses' => 'UserController@testing'));
// Route::get('/stats', array('uses' => 'UserController@stats'));

Route::get('/migrate', function() {
  Artisan::call('migrate', array('--force' => true));
});
Route::get('/rollback', function() {
   Artisan::call('migrate:rollback', array('--force' => true));
});
