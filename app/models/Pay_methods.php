<?php

class Pay_method extends Eloquent {

	protected $table = 'pay_methods';

	/*CREATE PAY*/
	public static function createPay_method($pay_method){
		$pay_method->name = Input::get('name');

		try{
			$pay_method->save();
			return true; /*redireccionamos a /dashboard*/
		}
		catch(Exception $e)
		{
			return false;
		}
	}

	/*VALIDATE PAY*/
	public static function validatePay_method(){
		$rules = array(
			'name' => 'required|unique:pay_methods,name'
		);

		$validator = Validator::make(Input::all(), $rules);
		return $validator;
	}

	/*VALIDATE UPDATE PAYMENT*/
	public static function validateUpdate($pay_method){
		$rules = array(
			'name'							=> 'required'
		);
		if( Input::get('name') !== $pay_method->name)
		{
			$rules['name'] = 'unique:pay_methods';
		}

		$validator = Validator::make(Input::all(), $rules);

		return $validator;
	}

	/*UPDATE PAYMENT*/

	public static function updatePay_method($pay_method){
		$pay_method->name 	= Input::get('name');

		try
		{
			$pay_method->save();
			return true;
		}
		catch(Exception $e)
		{
			return false;
		}
	}

	public function operations(){
		return $this->hasMany('Operation', 'cod_method');
	}
}