<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Auth\Reminders\RemindableTrait;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use RemindableTrait;

	public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	/*LEVEL USER*/
	public static function userLevel(){
		if(Auth::check()){
			$userLevel=Auth::user()->level;
			return $userLevel;
		}
	}

	/*CREATE USER*/
	public static function createUser($user){
		$user->name = Input::get('name');
		$user->email = Input::get('email');
		$user->password = Hash::make(Input::get('password'));
		$user->level = Input::get('level');

		if($user->level==null){
			$user->level=0;
		}

		try{
			$user->save();
			Auth::login($user); /*creamos una variable de autenticación*/
			return true; /*redireccionamos a /dashboard*/
		}
		catch(Exception $e)
		{
			return false;
		}
	}

	/*VALIDATE USER*/
	public static function validateUser(){
		$rules = array(
			'name' => 'required',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|confirmed|min:6',
			'password_confirmation' => 'required|same:password'
		);

		$validator = Validator::make(Input::all(), $rules);
		return $validator;
	}

	/*VALIDATE UPDATE USER*/
	public static function validateUpdate($user){
		$rules = array(
			'name'							=> 'required',
			'new_password'					=> 'confirmed|min:6',
			'new_password_confirmation'	=> 'same:new_password'
		);
		if( Input::has('email') && Input::get('email') !== $user->email)
		{
			$rules['email'] = 'required|unique:users,email';
		}
		else
		{
			$rules['email'] = 'required';
		}

		$validator = Validator::make(Input::all(), $rules);

		return $validator;
	}

	/*UPDATE USER*/

	public static function updateUser($user){
		$user->name 	= Input::get('name');
		$user->email	= Input::get('email');
		$user->level = Input::get('level');

		if($user->level==null){
			$user->level=0;
		}

		if( Input::has('new_password') && Input::get('new_password') !== '' )
			{
				$user->password	= Hash::make(Input::get('new_password'));
			}
		try
		{
			$user->save();
			return true;
		}
		catch(Exception $e)
		{
			return false;
		}
	}
	
	/*UPDATE USER LEVEL*/

	public static function updateUserLevel($user){
		$user->level = Input::get('level');
		try
		{
			$user->save();
			return true;
		}
		catch(Exception $e)
		{
			return false;
		}
	}



	public function operations()
	{
		return $this->hasMany('Operation', 'cod_user');
	}
	
	public function travels()
	{
		return $this->hasMany('Travel', 'cod_user');
	}

}