<?php

class Country extends Eloquent {

	protected $table = 'country';
	
	public function city(){
		return $this->hasMany('City', 'CountryCode');
	}
}