<?php

class City extends Eloquent {

	protected $table = 'city';
	
	public function country(){
		return $this->belongsTo('Country');
	}	
	public function operations(){
		return $this->hasMany('Operation', 'cod_city');
	}
}