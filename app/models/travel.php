<?php

class Travel extends Eloquent {

	// protected $table = 'travels';

	/*CREATE TRAVEL*/
	public static function createTravel($travel){
		$travel->name = Input::get('name');
		$travel->start = Input::get('start');
		$travel->finish = Input::get('finish');
		$travel->cash_balance = Input::get('cash_balance');
		$travel->tdc_balance = Input::get('tdc_balance');
		$travel->tdd_balance = Input::get('tdd_balance');
		$travel->cod_user = Auth::user()->id;

		try{
			$travel->save();
			return true; /*redireccionamos a /dashboard*/
		}
		catch(Exception $e)
		{
			return false;
		}
	}

	/*VALIDATE TRAVEL*/
	public static function validateTravel(){
		$rules = array(
			'name' => 'required',
			'start' => 'required',
			'finish' => 'required',
			'cash_balance' => 'required|numeric',
			'tdc_balance' => 'required|numeric',
			'tdd_balance' => 'required|numeric',
		);

		$validator = Validator::make(Input::all(), $rules);
		return $validator;
	}

	/*VALIDATE UPDATE TRAVEL*/
	public static function validateUpdate($travel){
		$rules = array(
			'name' => 'required',
			'start' => 'required',
			'finish' => 'required',
			'cash_balance' => 'required',
			'tdc_balance' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		return $validator;
	}

	/*UPDATE TRAVEL*/

	public static function updateTravel($travel){
		$travel->name = Input::get('name');
		$travel->start = Input::get('start');
		$travel->finish = Input::get('finish');
		$travel->cash_balance = Input::get('cash_balance');
		$travel->tdc_balance = Input::get('tdc_balance');
		$travel->tdd_balance = Input::get('tdd_balance');

		try{
			$travel->save();
			return true; /*redireccionamos a /dashboard*/
		}
		catch(Exception $e)
		{
			return false;
		}
	}

	public function operations(){
		return $this->hasMany('Operation', 'cod_travel');
	}
	public function users(){
		return $this->belongsTo('User');
	}
}