<?php

class Category extends Eloquent {

	protected $table = 'categories';

	/*CREATE CATEGORY*/
	public static function createCategory($category){
		$filepath="assets/img/icons/categories/";
		$category->name = Input::get('name');
		$category->sort = Input::get('sort');

		if(Input::file('icon')!=''){
			$category->pic = $filepath . Input::file('icon')->getClientOriginalName();
			$file=Input::file('icon');
		}
		try{
			if(Input::file('icon')!=''){
				$file->move('public/' . $filepath ,$file->getClientOriginalName());
			}
			$category->save();
			return true; /*redireccionamos a /dashboard*/
		}
		catch(Exception $e)
		{
			return false;
		}
	}

	/*VALIDATE CATEGORY*/
	public static function validateCategory(){
		$rules = array(
			'name' => 'required|unique:categories,name'
		);

		$validator = Validator::make(Input::all(), $rules);
		return $validator;
	}

	/*VALIDATE UPDATE CATEGORY*/
	public static function validateUpdate($category){
		$rules = array(
			'name' => 'required'
		);
		if( Input::get('name') !== $category->name)
		{
			$rules['name'] = 'unique:categories';
		}

		$validator = Validator::make(Input::all(), $rules);

		return $validator;
	}

	/*UPDATE CATEGORY*/

	public static function updateCategory($category){
		$filepath="assets/img/icons/categories/";
		$category->name 	= Input::get('name');
		$category->sort 	= Input::get('sort');

		if(Input::file('icon')!=''){
			$category->pic = $filepath . Input::file('icon')->getClientOriginalName();
			$file=Input::file('icon');
		}
		try
		{
			if(Input::file('icon')!=''){
				$file->move('public/' . $filepath ,$file->getClientOriginalName());
			}
			$category->save();
			return true;
		}
		catch(Exception $e)
		{
			return false;
		}
	}	

	/*REMOVE PIC CATEGORY*/

	public static function removePicCategory($category){
		File::delete(public_path()."/".$category->pic);
		$category->pic 	= '';
		try
		{
			$category->save();
			return true;
		}
		catch(Exception $e)
		{
			return false;
		}
	}	


	public function commerces(){
		return $this->hasMany('Commerce', 'cod_category');
	}
}