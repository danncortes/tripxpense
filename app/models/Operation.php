<?php

class Operation extends Eloquent {

	protected $table = 'operations';

	/*CREATE OPERATION*/
	public static function createOperation($operation){
		$operation->title = Input::get('title');
		$operation->description = Input::get('description');
		$operation->cost = Input::get('cost');
		$operation->cod_method = Input::get('cod_method');
		$operation->cod_commerce = Input::get('cod_commerce');
		$operation->neighborhood = Input::get('neighborhood');
		$operation->cod_city = Input::get('cod_city');
		$operation->date_op = Input::get('date_op');
		$operation->cod_travel = Input::get('cod_travel');
		$operation->cod_user = Auth::user()->id;
		try{
			$operation->save();
			return true; /*redireccionamos a /dashboard*/
		}
		catch(Exception $e)
		{
			return false;
		}
	}

	/*VALIDATE OPERATION*/
	public static function validateOperation(){
		$rules = array(
			'cost' => 'required|numeric',
			'cod_method' => 'required',
			'cod_commerce' => 'required',
			// 'cod_city' => 'required',
			'date_op' => 'required',
			'cod_travel' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		return $validator;
	}

	public static function statsOperations($rangeStat){
		$authUser=Auth::user()->id;
		$categories = Category::all();
		$statsOpe=array();
		$totalSpent=0;
		
		if($rangeStat!= 'all'){
			$travel = Travel::where('cod_user', $authUser)->where('id', $rangeStat)->get();
			$travel = $travel[0]->id;
		}

		$icat=0;
		foreach ($categories as $category) {
			$statsOpe[$icat]=array('name'=>$category->name);
			$commerces = $category->commerces;
			$costXcat=0;
			$numPurchase=0;
			foreach ($commerces as $commerce){

				if($rangeStat== 'all'){
					$operationsXcomm = Operation::where('cod_commerce', $commerce->id)->where('cod_user', $authUser)->get();
				}
				else{
					$operationsXcomm = Operation::where('cod_commerce', $commerce->id)->where('cod_user', $authUser)->where('cod_travel', $travel)->get();
				}
				$numPurchase=$numPurchase+$operationsXcomm->count();
				$costXcat=$costXcat+$operationsXcomm->sum('cost');
			}
			$statsOpe[$icat]['spent']=$costXcat;
			$statsOpe[$icat]['purchases']=$numPurchase;
			$totalSpent=$totalSpent+$costXcat;
			$icat++;
		}
		$ipercent=0;
		foreach($statsOpe as $theComm){
			if($totalSpent!=0)
			{
				$percent=$theComm['spent']*100/$totalSpent;
			}
			else{
				$percent=0;
			}
			$statsOpe[$ipercent]['percent']=round($percent);
			$ipercent++;
		}
		return $statsOpe;

	}

	public static function statsOperations2($rangeStat){
		$authUser=Auth::user()->id;
		$categories = Category::all();
		$statsOpe=array();
		
		if($rangeStat!= 'all'){
			$travel = Travel::where('cod_user', $authUser)->where('id', $rangeStat)->get();
			$travel = $travel[0]->id;
			$totalSpent=Operation::where('cod_user', $authUser)->where('cod_travel', $travel)->sum('cost');
		}
		else{
			$totalSpent=Operation::where('cod_user', $authUser)->sum('cost');
		}


		$statsOpe[0]=array('name'=>'spent');
		$statsOpe[1]=array('name'=>'purchases');
		$statsOpe[2]=array('name'=>'percent');

		$icat=0;
		foreach ($categories as $category) {
			$statsOpe[0]['categories'][$icat]['name']=$category->name;
			$statsOpe[1]['categories'][$icat]['name']=$category->name;
			$statsOpe[2]['categories'][$icat]['name']=$category->name;

			$commerces = $category->commerces;
			$costXcat=0;
			$numPurchase=0;
			
			foreach ($commerces as $commerce){

				if($rangeStat== 'all'){
					$operationsXcomm = Operation::where('cod_commerce', $commerce->id)->where('cod_user', $authUser)->get();
				}
				else{
					$operationsXcomm = Operation::where('cod_commerce', $commerce->id)->where('cod_user', $authUser)->where('cod_travel', $travel)->get();
				}
				$numPurchase=$numPurchase+$operationsXcomm->count();
				$costXcat=$costXcat+$operationsXcomm->sum('cost');
			}
			
			if($totalSpent == 0){
				$percent=0;
			}
			else{
				$percent=$costXcat*100/$totalSpent;
			}
			$statsOpe[0]['categories'][$icat]['number']=$costXcat;
			$statsOpe[1]['categories'][$icat]['number']=$numPurchase;
			$statsOpe[2]['categories'][$icat]['number']=round($percent, 2);
			
			$icat++;
		}
		
		return $statsOpe;

	}


	public static function getBalancePerTravel($scopeBalance){

		$authUser=Auth::user()->id;

		$totalCost=array();

		if($scopeBalance == 'all'){
			$travels = Travel::where('cod_user', $authUser)->get();
		}else{
			$travels = Travel::where('cod_user', $authUser)->where('id', $scopeBalance)->get();
		}

		$i=0;
		foreach ($travels as $travel) {
			$cod_travel=$travel->id;
			$name_travel=$travel->name;

			$type_methods=Pay_method::all();
			$totalCost[$i]=array('id'=>$cod_travel);
			$totalCost[$i]['name']=$name_travel;
			$ii=0;
			foreach ($type_methods as $method){
				$name_method=$method->name;
				$totalSpent = DB::table('operations')
                     ->where('cod_travel', $cod_travel)
                     ->where('cod_method', $method->id)
                     ->sum('cost');
				if($name_method=='CREDIT CARD'){
					$initial=$travel->tdc_balance;
					$remaining=$travel->tdc_balance - $totalSpent;
				}
				else if($name_method=='DEBIT CARD'){
					$initial=$travel->tdd_balance;
					$remaining=$travel->tdd_balance - $totalSpent;
				}
				else if($name_method=='CASH'){
					$initial=$travel->cash_balance;
					$remaining=$travel->cash_balance - $totalSpent;
				}
				if($initial!=0){
					$percent_spent=round(($totalSpent*100)/$initial, 2);
					$percent_remaining=round(($remaining*100)/$initial, 2);
				}
				else{
					$percent_spent=0;
					$percent_remaining=0;
				}

				$totalCost[$i]['paymethods'][$ii]['name']=strtolower($name_method);
				$totalCost[$i]['paymethods'][$ii]['initial']=$initial;
				$totalCost[$i]['paymethods'][$ii]['spent']=$totalSpent;
				$totalCost[$i]['paymethods'][$ii]['percent_spent']=$percent_spent;
				$totalCost[$i]['paymethods'][$ii]['remaining']=$remaining;
				$totalCost[$i]['paymethods'][$ii]['percent_remaining']=$percent_remaining;

				$ii++;
			}
			// $totalCost[$i]['cc-initial'][$i]=$travel->tdc_balance;
			// $totalCost[$i]['cs-initial'][$i]=$travel->cash_balance;
			$i++;

		}
		return $totalCost;

	}



	public static function travelBalances(){
		
	}


	
	public function users(){
		return $this->belongsTo('User', 'id');
	}	
	public function pay_methods(){
		return $this->belongsTo('Pay_method', 'id');
	}
	public function travels(){
		return $this->belongsTo('Travel', 'id');
	}
	public function city(){
		return $this->belongsTo('City', 'id');
	}
	public function commerces(){
		return $this->belongsTo('Commerce', 'id');
	}
}