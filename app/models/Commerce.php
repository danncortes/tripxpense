<?php

class Commerce extends Eloquent {

	protected $table = 'commerces';

	/*CREATE COMMERCE*/
	public static function saveCommerce($commerce){
		$commerce->name = Input::get('name');
		$commerce->cod_category = Input::get('cod_category');

		try{
			$commerce->save();
			return true; /*redireccionamos a /dashboard*/
		}
		catch(Exception $e)
		{
			return false;
		}
	}

	/*VALIDATE COMMERCE*/
	public static function validateCommerce(){

		$rules = array(
			'name' => 'required',
			'cod_category' => 'required'
		);
		$validator = Validator::make(Input::all(), $rules);
		return $validator;
	}
	

	/*VALIDATE UPDATE COMMERCE*/
	public static function validateUpdate($commerce){
		$rules = array(
			'name' => 'required',
			'cod_category' => 'required'
		);
		$validator = Validator::make(Input::all(), $rules);

		return $validator;
	}

	/*UPDATE COMMERCE*/

	public static function updateCommerce($commerce){
		return Commerce::createCommerce($commerce);
		// $commerce->name = Input::get('name');
		// $commerce->cod_category = Input::get('cod_category');

		// try
		// {
		// 	$commerce->save();
		// 	return true;
		// }
		// catch(Exception $e)
		// {
		// 	return false;
		// }
	}

	/*QUICK CREATE COMMERCE*/
	public static function saveQuickCommerce($commerce){

		try{
			$commerce->save();
			return true; /*redireccionamos a /dashboard*/
		}
		catch(Exception $e)
		{
			return false;
		}
	}

	public function categories(){
		return $this->belongsTo('Category', 'id');
	}
	public function operations(){
		return $this->hasMany('Operation', 'cod_commerce');
	}
}