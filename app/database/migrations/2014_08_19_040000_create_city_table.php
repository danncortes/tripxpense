<?php

use Illuminate\Database\Migrations\Migration;

class CreateCityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('city', function($table){
			$table->string('ID')->primary();
			$table->string('Name', 35)->default('');
			$table->string('CountryCode', 3)->default('');
			$table->string('District', 20)->default('');
			$table->integer('Population')->unsigned();
			$table->foreign('CountryCode')->references('code')->on('country')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('city');
	}

}