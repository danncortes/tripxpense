<?php

use Illuminate\Database\Migrations\Migration;

class CreateTravelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('travels', function($table)
		{
			$table->increments('id');
			$table->string('name', 35);
			$table->date('start');
			$table->date('finish');
			$table->decimal('cash_balance',6,2)->default(0);
			$table->decimal('tdc_balance',6,2)->default(0);
			$table->decimal('tdd_balance',6,2)->default(0);
			$table->integer('cod_user')->unsigned();
			
			$table->foreign('cod_user')->references('id')->on('users')->onDelete('cascade');
			$table->timestamps();
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('travels');
	}

}