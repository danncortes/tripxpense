<?php

use Illuminate\Database\Migrations\Migration;

class CreateOperationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('operations', function($table)
		{
			$table->increments('id');
			$table->string('title', 40);
			$table->text('description');
			$table->decimal('cost',6,2);
			$table->date('date_op');
			$table->integer('cod_method')->unsigned();
			$table->integer('cod_commerce')->unsigned();
			$table->string('neighborhood')->default('');
			$table->integer('cod_city')->unsigned();
			$table->integer('cod_travel')->unsigned();
			$table->integer('cod_user')->unsigned();

			$table->foreign('cod_method')->references('id')->on('pay_methods')->onDelete('cascade');
			$table->foreign('cod_commerce')->references('id')->on('commerces')->onDelete('cascade');
			$table->foreign('cod_travel')->references('id')->on('travels')->onDelete('cascade');
			$table->foreign('cod_user')->references('id')->on('users')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('operations');
	}

}