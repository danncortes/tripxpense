<?php

use Illuminate\Database\Migrations\Migration;

class CreatingCountryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('country', function($table){
			$table->string('code', 3)->default('')->primary();
			$table->string('Name', 52)->default('');
			$table->enum('Continent', array('Asia','Europe','North America','Africa','Oceania','Antarctica','South America'))->default('Asia');
			$table->string('Region', 26)->default('');
			$table->float('SurfaceArea', 10,2)->default('0.00');
			$table->smallInteger('IndepYear')->unsigned()->default(0);
			$table->integer('Population')->unsigned()->default(0);
			$table->float('LifeExpectancy', 3,1)->nullable();
			$table->float('GNP', 10,2)->nullable();
			$table->float('GNPOld', 10,2)->nullable();
			$table->string('LocalName', 45)->default('');
			$table->string('GovernmentForm', 45)->default('');
			$table->string('HeadOfState', 60)->default('');
			$table->integer('Capital')->unsigned()->nullable();
			$table->string('Code2', 2)->default('');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('Country');
	}

}