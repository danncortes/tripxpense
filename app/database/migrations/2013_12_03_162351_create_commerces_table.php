<?php

use Illuminate\Database\Migrations\Migration;

class CreateCommercesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('commerces', function($table)
		{
			$table->increments('id');
			$table->string('name', 35);
			$table->integer('cod_category')->unsigned();
			$table->foreign('cod_category')->references('id')->on('categories')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('commerces');
	}

}