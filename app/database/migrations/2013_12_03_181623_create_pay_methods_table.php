<?php

use Illuminate\Database\Migrations\Migration;

class CreatePayMethodsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pay_methods', function($table)
		{
			$table->increments('id');
			$table->string('name', 35);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pay_methods');
	}

}